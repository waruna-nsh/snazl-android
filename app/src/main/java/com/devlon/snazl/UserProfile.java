package com.devlon.snazl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jess.ui.TwoWayGridView;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.okhttp.Response;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserProfile extends AppCompatActivity {

    Drawable myDrawable;
    TextView toolbar_title, tv_no_following;
    Typeface custom_font;

    RecentAdapter recentAdapter;
    TwoWayGridView gridview,gridview_following;
    ArrayList<String> snazlTextLists = new ArrayList<String>();
    Button btnFollow;
    NetworkConnection nw;
    ProgressDialog progressDialog;
    ProgressDialog progressDialog_new;
    Boolean netConnection = false;
    RelativeLayout rel_follow;

    ArrayList<String> user_id_list = new ArrayList<>();
    ArrayList<String> user_name_list = new ArrayList<>();
    ArrayList<String> user_email_list = new ArrayList<>();

    ArrayList<String> user_id_list_follwing = new ArrayList<>();
    ArrayList<String> user_name_list_follwing = new ArrayList<>();
    ArrayList<String> user_email_list_follwing = new ArrayList<>();

    SessionManager sm;

    FollowingAdapter followingAdapter;
    FollowerAdapter followerAdapter;
    ProgressBar pb_following;
    Response response;
    String snazl_user_id, user_id, fileUrl, fileName, userName, status_profile, user_different_name;
    ImageView iv_snazl_profile;
    private static int RESULT_LOAD_IMAGE = 1;
    EditText et_status, et_username;
    TextView tv_fullname;
    TextView tv_folowernumber;

    ProgressBar download;


    ImageLoader imageLoader;
    DisplayImageOptions options;

    ArrayList<String> arrayList_foolower_snazl;

    KeyListener variable1;
    KeyListener variable2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setTitle("");
        myToolbar.setTitleTextColor(Color.WHITE);

        nw = new NetworkConnection(UserProfile.this);
        sm = new SessionManager(UserProfile.this);

        arrayList_foolower_snazl=new ArrayList<String>();

        HashMap<String,String> userDetails = sm.getUserDetails();
        user_id = userDetails.get(SessionManager.KEY_ID);
        userName = userDetails.get(SessionManager.KEY_NAME);

        snazl_user_id = getIntent().getStringExtra("snazl_user_id");

        progressDialog = new ProgressDialog(UserProfile.this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        progressDialog_new = new ProgressDialog(UserProfile.this);
        progressDialog_new.setMessage("Please Wait...");
        progressDialog_new.setCancelable(false);

        new ViewUser().execute();

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            myDrawable = getApplicationContext().getDrawable(R.drawable.ic_overflow);
        } else {
            myDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_overflow);
        }

        myToolbar.setOverflowIcon(myDrawable);

        custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");

        toolbar_title.setTypeface(custom_font);
        toolbar_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rel_follow = (RelativeLayout) findViewById(R.id.rel_follow);
        pb_following = (ProgressBar) findViewById(R.id.pb_following);
        tv_no_following= (TextView) findViewById(R.id.tv_no_following);

        iv_snazl_profile = (ImageView) findViewById(R.id.iv_snazl_profile);
        et_status = (EditText) findViewById(R.id.et_status);
        et_username = (EditText) findViewById(R.id.et_username);
        tv_fullname = (TextView) findViewById(R.id.tv_fullname);
        download = (ProgressBar) findViewById(R.id.download);

        tv_folowernumber = (TextView) findViewById(R.id.tv_folowernumber);

        variable1 = et_status.getKeyListener();
        variable2 = et_username.getKeyListener();

        snazlTextLists.add("one");
        snazlTextLists.add("two");
        snazlTextLists.add("three");
        snazlTextLists.add("four");
        snazlTextLists.add("five");
        snazlTextLists.add("six");
        snazlTextLists.add("seven");
        snazlTextLists.add("eight");
        snazlTextLists.add("nine");
        snazlTextLists.add("ten");
        snazlTextLists.add("eleven");
        snazlTextLists.add("twelve");
        snazlTextLists.add("thirteen");
        snazlTextLists.add("fourteen");
        snazlTextLists.add("fifteen");
        snazlTextLists.add("sixteen");
        snazlTextLists.add("seventeen");
        snazlTextLists.add("eighteen");
        snazlTextLists.add("nineteen");
        snazlTextLists.add("twenty");

        gridview_following = (TwoWayGridView) findViewById(R.id.gridview_following);

        gridview = (TwoWayGridView) findViewById(R.id.gridview);
        recentAdapter = new RecentAdapter(getApplicationContext(),snazlTextLists);
       // gridview.setAdapter(recentAdapter);

        btnFollow = (Button) findViewById(R.id.btnFollow);

        if (user_id.equals(snazl_user_id)){
            btnFollow.setVisibility(View.GONE);

            et_status.setKeyListener(variable1);
            et_username.setKeyListener(variable2);


        }else {
            btnFollow.setVisibility(View.VISIBLE);
            et_status.setKeyListener(null);
            et_username.setKeyListener(null);
        }

        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FollowOperation().execute();   // when click on follow
            }
        });

        rel_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FollowersOperation().execute();
            }
        });

        new FollowersOperation().execute();
        new FollowingOperation().execute();

        iv_snazl_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                status_profile = et_status.getText().toString().trim();
                user_different_name = et_username.getText().toString();

                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_signOut) {
            sm.logOutUser();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class FollowOperation extends AsyncTask<String, Void, Void>{

        String status, message, totalFollowers;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("follower_user_id",user_id));
                    nameValuePairs.add(new BasicNameValuePair("follower_follower_id", snazl_user_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"follow.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");
                    message = js.getString("message");

                    if (status.equals("Success")){
                        totalFollowers = js.getString("totalFollowers");
                    }

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;
            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(UserProfile.this, "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){
                    if (message.equals("You are succesfully followed")){
                        btnFollow.setText("Following");
                    }else {
                        btnFollow.setText("Follow");
                    }

                }else {
                    Toast toast = Toast.makeText(UserProfile.this, message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }

    public class FollowersOperation extends AsyncTask<String, Void, Void>{

        String status, message;
        int follower_size;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            arrayList_foolower_snazl.clear();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("user_id", snazl_user_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"followerlist.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");

                    if (status.equals("success")){
                        JSONArray follower_list = js.getJSONArray("follower_list");

                        follower_size=follower_list.length();
                        for (int i=0 ; i<=follower_list.length() ; i++){
                            JSONObject jo = follower_list.getJSONObject(i);
                            String user_id = jo.getString("user_id");
                            String user_name = jo.getString("user_name");
                            String user_email_address = jo.getString("user_email_address");
                            String snazl_final_url = jo.getString("snazl_final_url");

                            user_id_list.add(user_id);
                            user_name_list.add(user_name);
                            user_email_list.add(user_email_address);
                            arrayList_foolower_snazl.add(snazl_final_url);
                        }
                    }else if (status.equals("Fail")){
                        message = js.getString("message");
                    }

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;
            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(UserProfile.this, "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("success")){

                    tv_folowernumber.setText(String.valueOf(follower_size));

                    followerAdapter = new FollowerAdapter(UserProfile.this,arrayList_foolower_snazl);
                    gridview.setAdapter(followerAdapter);


                    System.out.println("Followers Lists : "+user_name_list);
                }else {
                    Toast toast = Toast.makeText(UserProfile.this, message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }

    public class FollowingOperation extends AsyncTask<String, Void, Void>{

        String status, message;

        @Override
        protected void onPreExecute() {
            pb_following.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("user_id",snazl_user_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"followinglist.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");

                    if (status.equals("success")){
                        JSONArray follower_list = js.getJSONArray("following_list");
                        for (int i=0 ; i<=follower_list.length() ; i++){
                            JSONObject jo = follower_list.getJSONObject(i);
                            String user_id = jo.getString("user_id");
                            String user_name = jo.getString("user_name");
                            String user_email_address = jo.getString("user_email_address");

                            user_id_list_follwing.add(user_id);
                            user_name_list_follwing.add(user_name);
                            user_email_list_follwing.add(user_email_address);
                        }
                    }else if (status.equals("Fail")){
                        message = js.getString("message");
                    }

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;
            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            pb_following.setVisibility(View.GONE);

            if (!netConnection){
                Toast toast = Toast.makeText(UserProfile.this, "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("success")){
                    System.out.println("Followers Lists : "+user_name_list_follwing);

                    followingAdapter = new FollowingAdapter(UserProfile.this,user_name_list_follwing);
                    gridview_following.setAdapter(followingAdapter);

                }else {
                    if (message.equals("No following found")){
                        tv_no_following.setVisibility(View.VISIBLE);
                    }else {
                        Toast toast = Toast.makeText(UserProfile.this, message, Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
            }

            super.onPostExecute(aVoid);
        }
    }

    public class EditProfile extends AsyncTask<String,Void,Void>{

        String status, message;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {
                    ServiceHandler sh  = new ServiceHandler();
                    if (fileUrl != null){
                        JSONObject commentAdd = new JSONObject();

                        String newUrl = Constant.apiUrl +"?method=updateuser&name="+user_different_name.replace(" ","%20")+"&fullname="+userName.replace(" ","%20")+"&text_status="+status_profile.replace(" ","%20") +"&user_id="+user_id;

                        System.out.println("upload url : "+newUrl);

                        response = sh.uploadImageFile(newUrl,fileUrl,"user_image",commentAdd.toString(),fileName);

                        JSONObject js = new JSONObject(response.body().string());
                        status = js.getString("status");
                        Log.e("status add comment", status);

                        if (status.equals("Success")){

                            JSONObject jo = js.getJSONObject("user_info");
                            String user_id = jo.getString("user_id");
                            String user_full_name = jo.getString("user_full_name");
                            String user_name = jo.getString("user_name");
                            String user_email_address = jo.getString("user_email_address");
                            String user_text_status = jo.getString("user_text_status");
                            String user_image_url = jo.getString("user_image_url");


                        }else if (status.equals("Fail")){
                            message = js.getString("message");
                        }

                    }else {
                        List<NameValuePair> nameValuePairs = new ArrayList<>();
                        nameValuePairs.add(new BasicNameValuePair("method", "updateuser"));
                        nameValuePairs.add(new BasicNameValuePair("user_id", user_id));
                        nameValuePairs.add(new BasicNameValuePair("name", user_different_name));
                        nameValuePairs.add(new BasicNameValuePair("fullname", userName));
                        nameValuePairs.add(new BasicNameValuePair("text_status", status_profile));
                        nameValuePairs.add(new BasicNameValuePair("user_id", user_id));

                        String response = sh.makeServiceCall(Constant.apiUrl, ServiceHandler.GET, nameValuePairs);

                        Log.e("Response : ", response);

                        JSONObject js = new JSONObject(response);
                        status = js.getString("status");

                        if (status.equals("Success")) {

                            JSONObject jo = js.getJSONObject("user_info");
                            String user_id = jo.getString("user_id");
                            String user_full_name = jo.getString("user_full_name");
                            String user_name = jo.getString("user_name");
                            String user_email_address = jo.getString("user_email_address");
                            String user_text_status = jo.getString("user_text_status");
                            String user_image_url = jo.getString("user_image_url");


                        } else if (status.equals("Fail")) {
                            message = js.getString("message");
                        }
                    }

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;
            }else {
                netConnection = false;
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(UserProfile.this, "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){

                    Toast toast = Toast.makeText(UserProfile.this, "Profile updated successfully.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                }else {
                    Toast toast = Toast.makeText(UserProfile.this, message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            fileUrl = cursor.getString(columnIndex);

            cursor.close();

            String[] filePathColumn1 = {MediaStore.Images.Media.DISPLAY_NAME};
            Cursor cursor1 = getContentResolver().query(selectedImage,
                    filePathColumn1, null, null, null);
            cursor1.moveToFirst();
            int columnIndex1 = cursor1.getColumnIndex(filePathColumn1[0]);
            fileName = cursor1.getString(columnIndex1);
            System.out.println("File Url : " + fileName);
            cursor1.close();

            iv_snazl_profile.setImageURI(selectedImage);

            new EditProfile().execute();
        }
    }


    // user view by user id

    public class ViewUser extends AsyncTask<String, Void, Void> {

        String status, message, user_id_api, user_full_name,user_name,user_email_address,user_text_status,user_image_url;

        @Override
        protected void onPreExecute() {
            progressDialog_new.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("userid", snazl_user_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"viewprofile.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ", response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");

                    if (status.equals("Success")){
                        JSONObject jo = js.getJSONObject("User_info");
                        user_id_api = jo.getString("user_id");
                        user_full_name = jo.getString("user_full_name");
                        user_name = jo.getString("user_name");
                        user_email_address = jo.getString("user_email_address");
                        user_text_status = jo.getString("user_text_status");
                        user_image_url = jo.getString("user_image_url");
                    }else if (status.equals("Fail")){
                        message = js.getString("message");
                    }

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;
            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog_new.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(UserProfile.this, "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){

                    tv_fullname.setText(user_full_name);
                    et_status.setText(user_text_status);
                    et_username.setText(user_name);

                    DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                            .cacheOnDisc(true).cacheInMemory(true)
                            .imageScaleType(ImageScaleType.EXACTLY)
                            .displayer(new FadeInBitmapDisplayer(300)).build();

                    ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                            .defaultDisplayImageOptions(defaultOptions)
                            .memoryCache(new WeakMemoryCache())
                            .discCacheSize(100 * 1024 * 1024).build();
                    ImageLoader.getInstance().init(config);

                    imageLoader = ImageLoader.getInstance();
                    options = new DisplayImageOptions.Builder()
                            .cacheInMemory(true)
                            .cacheOnDisc(true)
                            .resetViewBeforeLoading(true).build();

                    imageLoader.getInstance().displayImage(user_image_url, iv_snazl_profile, options, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            download.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            String message = null;
                            switch (failReason.getType()) {
                                case IO_ERROR:
                                    //message = "Input/Output error";
                                    System.out.println("IO_ERROR");
                                    //  holder.img_Product.setImageResource(R.drawable.no_image);
                                    break;
                                case DECODING_ERROR:
                                    //message = "Image cannot be decoded...";
                                    System.out.println("DECODING_ERROR");
                                    //  holder.img_Product.setImageResource(R.drawable.no_image);
                                    break;
                                case NETWORK_DENIED:
                                    //message = "Downloads are denied";
                                    System.out.println("NETWORK_DENIED");
                                    // holder.img_Product.setImageResource(R.drawable.no_image);
                                    break;
                                case OUT_OF_MEMORY:
                                    //message = "Out Of Memory error";
                                    System.out.println("OUT_OF_MEMORY");
                                    //  holder.img_Product.setImageResource(R.drawable.no_image);
                                    break;
                                case UNKNOWN:
                                    //message = "Unknown error";
                                    System.out.println("UNKNOWN");
                                    // holder.img_Product.setImageResource(R.drawable.no_image);
                                    break;
                            }
                            download.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            //super.onLoadingComplete(imageUri, view, loadedImage);
                            download.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            super.onLoadingCancelled(imageUri, view);
                        }
                    });

                   /* Toast toast = Toast.makeText(UserProfile.this,  "User Details : "+user_full_name+"\n"+user_name+"\n"+user_email_address+"\n"+user_text_status+"\n"+user_image_url, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();*/
                }else {
                    Toast toast = Toast.makeText(UserProfile.this, message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }
}
