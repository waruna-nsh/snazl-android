package com.devlon.snazl;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by User on 7/13/2016.
 */
public class FollowingAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> textFollowingLists;

    public FollowingAdapter(Context context, ArrayList<String> textFollowingLists){
        this.context = context;
        this.textFollowingLists = textFollowingLists;
    }
    @Override
    public int getCount() {
        return textFollowingLists.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder{
        TextView tv_name;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.following_row, null);
            holder = new ViewHolder();

            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);


            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_name.setText(textFollowingLists.get(position));

        return convertView;
    }
}
