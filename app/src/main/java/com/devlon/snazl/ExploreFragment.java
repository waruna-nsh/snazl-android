package com.devlon.snazl;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jess.ui.TwoWayAdapterView;
import com.jess.ui.TwoWayGridView;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import at.markushi.ui.CircleButton;
import it.sephiroth.android.library.widget.AdapterView;
import it.sephiroth.android.library.widget.HListView;

/**
 * Created by User on 3/15/2016.
 */
public class ExploreFragment extends Fragment{

    HListView listSnazzy;
    TwoWayGridView gridview, gridview_interest;
    ArrayList<String> snazlTextLists = new ArrayList<String>();
    SnazzyAdapter snazzyAdapter;
    MySnazal_Recent_Adapter mySnazal_Recent_Adapter;
    RecentAdapter recentAdapter;
    InterestAdapter interestAdapter;
    TextView tv_add;
    EditText tv_linkOpt;
    Dialog dialog;
    Boolean isDialogShowing = false;
    String editedUrl="";
    ImageView iv_search;

    NetworkConnection nw;
    ProgressDialog progressDialog;
    ProgressDialog progressDialog_new;
    Boolean netConnection = false;
    ArrayList<String> snazzy_arraylist = new ArrayList<>();
    ArrayList<String> snazzy_image_arraylist = new ArrayList<>();

    ArrayList<String> snazzy_arraylist_recent = new ArrayList<>();
    ArrayList<String> snazzy_image_arraylist_recent = new ArrayList<>();

    ArrayList<Explore_Snazzy_Pojo> arralist_explore_snazzy_pojos=new ArrayList<Explore_Snazzy_Pojo>();
    ArrayList<Explore_Recent_Pojo> arralist_explore_recent_pojos=new ArrayList<Explore_Recent_Pojo>();

    SessionManager sm;
    String user_id, snazl_id;

    CircleButton cb_favourite,cb_like;
    CircleButton cb_favouritenew,cb_likenew;
    Boolean isFav = true, isLiked = true;
    Boolean isFavnew = true, isLikednew = true;
    String search_snazl, favToast;
    Explore_Snazzy_Pojo  esp;
    Explore_Recent_Pojo  erp;
    private long mLastClickTime = 0;
    private long mLastClickTime1 = 0;

    String report_message;

    String snazl_user_id;
    ProgressDialog progressDialog_report;
   Dialog dialogreport;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        snazzy_arraylist.clear();
        snazzy_image_arraylist.clear();

        snazzy_arraylist_recent.clear();
        snazzy_image_arraylist_recent.clear();
        arralist_explore_snazzy_pojos.clear();
        arralist_explore_recent_pojos.clear();
        return inflater.inflate(R.layout.fragment_explore, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int position = FragmentPagerItem.getPosition(getArguments());

        nw = new NetworkConnection(getActivity());

        sm = new SessionManager(getActivity());

        HashMap<String,String> userDetails = sm.getUserDetails();
        user_id = userDetails.get(SessionManager.KEY_ID);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        progressDialog_new = new ProgressDialog(getActivity());
        progressDialog_new.setMessage("Please Wait...");
        progressDialog_new.setCancelable(false);


        progressDialog_report = new ProgressDialog(getActivity());
        progressDialog_report.setMessage("Please Wait...");
        progressDialog_report.setCancelable(false);


        listSnazzy = (HListView) view.findViewById(R.id.listSnazzy);
        gridview = (TwoWayGridView) view.findViewById(R.id.gridview);
        gridview_interest = (TwoWayGridView) view.findViewById(R.id.gridview_interest);
        tv_add = (TextView) view.findViewById(R.id.tv_add);
        iv_search = (ImageView) view.findViewById(R.id.iv_search);

        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), InterestScreen.class);
                intent.putExtra("interests", "");
                startActivity(intent);

            }
        });

        snazlTextLists.add("one");
        snazlTextLists.add("two");
        snazlTextLists.add("three");
        snazlTextLists.add("four");
        snazlTextLists.add("five");
        snazlTextLists.add("six");
        snazlTextLists.add("seven");
        snazlTextLists.add("eight");
        snazlTextLists.add("nine");
        snazlTextLists.add("ten");
        snazlTextLists.add("eleven");
        snazlTextLists.add("twelve");
        snazlTextLists.add("thirteen");
        snazlTextLists.add("fourteen");
        snazlTextLists.add("fifteen");
        snazlTextLists.add("sixteen");
        snazlTextLists.add("seventeen");
        snazlTextLists.add("eighteen");
        snazlTextLists.add("nineteen");
        snazlTextLists.add("twenty");

        new MySnazzyOperation().execute();
        new MySnazzyRecentOperation().execute();

       /* recentAdapter = new RecentAdapter(getActivity(),snazlTextLists);
        gridview.setAdapter(recentAdapter);*/

        interestAdapter = new InterestAdapter(getActivity(),Constant.temp_interests);
        gridview_interest.setAdapter(interestAdapter);

        tv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_link);
                Window window = dialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.dialogbg);
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                wlp.gravity = Gravity.CENTER;
                window.setAttributes(wlp);

                isDialogShowing = true;
                final ArrayList<String> tempInterestsList = new ArrayList<String>();

                tv_linkOpt = (EditText) dialog.findViewById(R.id.tv_linkOpt);
                tv_linkOpt.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

                tv_linkOpt.setHint("Search keywords for your interest.");

                /*if (!editedUrl.isEmpty()) {
                    tv_linkOpt.setText(editedUrl);
                }*/

                tv_linkOpt.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View view, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            switch (keyCode) {

                                case KeyEvent.KEYCODE_ENTER:
                                    editedUrl = tv_linkOpt.getText().toString().trim();
                                    Constant.temp_interests.add(editedUrl);
                                    interestAdapter.notifyDataSetChanged();
                                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                    dialog.dismiss();
                            }
                        }
                        return false;
                    }
                });

                dialog.show();
            }
        });

        KeyboardVisibilityEvent.setEventListener(getActivity(), new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                // some code depending on keyboard visiblity status
                if (isOpen) {
                    Log.e("Keyboard", " " + isOpen);
                } else {
                    if (isDialogShowing) {
                        interestAdapter.notifyDataSetChanged();
                        dialog.dismiss();
                        isDialogShowing = false;
                    }
                }
            }
        });

        gridview_interest.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(),InterestScreen.class);
                intent.putExtra("interests",Constant.temp_interests.get(position));
                getActivity().startActivity(intent);
            }
        });

        // Snazzy   Explore_Snazzy_Pojo

        listSnazzy.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                ImageLoader imageLoader;
                DisplayImageOptions options;

               esp = arralist_explore_snazzy_pojos.get(i);

                snazl_id = esp.getSnazl_id();
                snazl_user_id=esp.getSnazl_user_id();

                final Dialog dialogEdit = new Dialog(getActivity());
                dialogEdit.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogEdit.setContentView(R.layout.open_snazl_explor_dialog);
                dialogEdit.getWindow().setBackgroundDrawableResource(R.color.transparent);

                Window window = dialogEdit.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
//                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                wlp.gravity = Gravity.CENTER;

                window.setAttributes(wlp);

                final ImageView iv_close = (ImageView) dialogEdit.findViewById(R.id.iv_close);
                final ImageView iv_snazl_invisible = (ImageView) dialogEdit.findViewById(R.id.iv_snazl_invisible);
                cb_favourite = (CircleButton) dialogEdit.findViewById(R.id.cb_favourite);
                cb_like = (CircleButton) dialogEdit.findViewById(R.id.cb_like);
                CircleButton cb_share = (CircleButton) dialogEdit.findViewById(R.id.cb_share);
                final TextView tv_snazl_text = (TextView) dialogEdit.findViewById(R.id.tv_snazl_text);
                final TextView tv_username = (TextView) dialogEdit.findViewById(R.id.tv_username);
                final Button btn_delete = (Button) dialogEdit.findViewById(R.id.btn_delete);
                final Button btn_report = (Button) dialogEdit.findViewById(R.id.btn_report);
                final ProgressBar download = (ProgressBar) dialogEdit.findViewById(R.id.download);

                DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                        .cacheOnDisc(true).cacheInMemory(true)
                        .imageScaleType(ImageScaleType.EXACTLY)
                        .displayer(new FadeInBitmapDisplayer(300)).build();

                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                        .defaultDisplayImageOptions(defaultOptions)
                        .memoryCache(new WeakMemoryCache())
                        .discCacheSize(100 * 1024 * 1024).build();
                ImageLoader.getInstance().init(config);

                imageLoader = ImageLoader.getInstance();
                options = new DisplayImageOptions.Builder()
                        .cacheInMemory(true)
                        .cacheOnDisc(true)
                        .resetViewBeforeLoading(true).build();

                imageLoader.getInstance().displayImage(esp.getSnazl_url(), iv_snazl_invisible, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        download.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        String message = null;
                        switch (failReason.getType()) {
                            case IO_ERROR:
                                //message = "Input/Output error";
                                System.out.println("IO_ERROR");
                                //  holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case DECODING_ERROR:
                                //message = "Image cannot be decoded...";
                                System.out.println("DECODING_ERROR");
                                //  holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case NETWORK_DENIED:
                                //message = "Downloads are denied";
                                System.out.println("NETWORK_DENIED");
                                // holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case OUT_OF_MEMORY:
                                //message = "Out Of Memory error";
                                System.out.println("OUT_OF_MEMORY");
                                //  holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case UNKNOWN:
                                //message = "Unknown error";
                                System.out.println("UNKNOWN");
                                // holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                        }
                        download.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        //super.onLoadingComplete(imageUri, view, loadedImage);
                        download.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        super.onLoadingCancelled(imageUri, view);
                    }
                });



                if (user_id.equals(snazl_id)){
                    btn_delete.setVisibility(View.VISIBLE);
                    btn_report.setVisibility(View.GONE);
                }else {
                    btn_delete.setVisibility(View.GONE);
                    btn_report.setVisibility(View.VISIBLE);
                }

                tv_snazl_text.setText(esp.getSnazl_title());

                iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogEdit.dismiss();
                    }
                });

                if (esp.getIsfav().equals("1")){
                    cb_favourite.setImageResource(R.drawable.fav_orange);
                    isFav = false;
                }else {
                    cb_favourite.setImageResource(R.drawable.fav);
                    isFav = true;
                }

                if (esp.getIslike().equals("1")){
                    cb_like.setImageResource(R.drawable.ic_liked);
                    isLiked = false;
                }else {
                    cb_like.setImageResource(R.drawable.ic_thumb);
                    isLiked = true;
                }

                cb_favourite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isFav) {
                            favToast = "Favorite";
                            cb_favourite.setImageResource(R.drawable.fav_orange);
                            new AddRemoveFavouriteExplore().execute();
                            isFav = false;
                            esp.setIsfav("1");
                        } else {
                            favToast = "Favorite Removed";
                            cb_favourite.setImageResource(R.drawable.fav);
                            new AddRemoveFavouriteExplore().execute();
                            isFav = true;
                            esp.setIsfav("0");
                        }

                    }
                });

                cb_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isLiked) {
                            cb_like.setImageResource(R.drawable.ic_liked);
                            isLiked = false;
                            esp.setIslike("1");
                        } else {
                            cb_like.setImageResource(R.drawable.ic_thumb);
                            isLiked = true;
                            esp.setIslike("0");
                        }

                        new AddRemoveLikeExplore().execute();
                    }
                });

                cb_share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Bitmap bitmap=((BitmapDrawable)iv_snazl_invisible.getDrawable()).getBitmap();

                        String pathofBmp = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap,"Snazy Image", null);
                        Uri bmpUri = Uri.parse(pathofBmp);
                        final Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                        shareIntent.setType("image/*");
                        startActivity(Intent.createChooser(shareIntent, "Share image using"));

                    }
                });


                btn_report.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialogreport = new Dialog(getActivity());
                        dialogreport.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogreport.setContentView(R.layout.report_send_dialog);
                        dialogreport.getWindow().setBackgroundDrawableResource(R.color.transparent);

                        Window window = dialogreport.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();
//                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        wlp.gravity = Gravity.CENTER;

                        window.setAttributes(wlp);

                        final ImageView iv_close_report = (ImageView) dialogreport.findViewById(R.id.iv_closereport);
                        final Button btn_sendreport = (Button) dialogreport.findViewById(R.id.btn_sendreport);
                        final EditText et_messagereport = (EditText) dialogreport.findViewById(R.id.et_messagereport);

                        iv_close_report.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                dialogreport.dismiss();
                            }
                        });


                        btn_sendreport.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                report_message=et_messagereport.getText().toString().trim();

                                if (report_message.isEmpty()){

                                    Toast.makeText(getActivity(),"Enter message",Toast.LENGTH_SHORT).show();

                                }else {

                                    new SendRepotExplore().execute();

                                //    Toast.makeText(getActivity(),"s_id : "+snazl_id+"  u_id : "+snazl_user_id,Toast.LENGTH_SHORT).show();


                                }
                            }
                        });

                        dialogreport.show();


                    }
                });


                tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), UserProfile.class);
                        intent.putExtra("snazl_user_id",snazl_user_id);
                        startActivity(intent);
                    }
                });

                dialogEdit.show();
            }
        });

        // Recent Snazl     Explore_Recent_Pojo

        gridview.setOnItemClickListener(new TwoWayAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {

                if (SystemClock.elapsedRealtime() - mLastClickTime1 < 1000){
                    return;
                }
                mLastClickTime1 = SystemClock.elapsedRealtime();

                ImageLoader imageLoader;
                DisplayImageOptions options;

                erp = arralist_explore_recent_pojos.get(position);

                snazl_id = erp.getSnazl_id();
                snazl_user_id=erp.getSnazl_user_id();

                final Dialog dialogEdit = new Dialog(getActivity());
                dialogEdit.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogEdit.setContentView(R.layout.open_snazl_explor_dialog);
                dialogEdit.getWindow().setBackgroundDrawableResource(R.color.transparent);

                Window window = dialogEdit.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
//                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                wlp.gravity = Gravity.CENTER;

                window.setAttributes(wlp);

                final ImageView iv_close = (ImageView) dialogEdit.findViewById(R.id.iv_close);
                final ImageView iv_snazl_invisible = (ImageView) dialogEdit.findViewById(R.id.iv_snazl_invisible);
                cb_favouritenew = (CircleButton) dialogEdit.findViewById(R.id.cb_favourite);
                cb_likenew = (CircleButton) dialogEdit.findViewById(R.id.cb_like);
                CircleButton cb_share = (CircleButton) dialogEdit.findViewById(R.id.cb_share);
                final TextView tv_snazl_text = (TextView) dialogEdit.findViewById(R.id.tv_snazl_text);
                final TextView tv_username = (TextView) dialogEdit.findViewById(R.id.tv_username);
                final Button btn_delete = (Button) dialogEdit.findViewById(R.id.btn_delete);
                final Button btn_report = (Button) dialogEdit.findViewById(R.id.btn_report);
                final ProgressBar download = (ProgressBar) dialogEdit.findViewById(R.id.download);

                DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                        .cacheOnDisc(true).cacheInMemory(true)
                        .imageScaleType(ImageScaleType.EXACTLY)
                        .displayer(new FadeInBitmapDisplayer(300)).build();

                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                        .defaultDisplayImageOptions(defaultOptions)
                        .memoryCache(new WeakMemoryCache())
                        .discCacheSize(100 * 1024 * 1024).build();
                ImageLoader.getInstance().init(config);

                imageLoader = ImageLoader.getInstance();
                options = new DisplayImageOptions.Builder()
                        .cacheInMemory(true)
                        .cacheOnDisc(true)
                        .resetViewBeforeLoading(true).build();

                imageLoader.getInstance().displayImage(erp.getSnazl_url(), iv_snazl_invisible, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        download.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        String message = null;
                        switch (failReason.getType()) {
                            case IO_ERROR:
                                //message = "Input/Output error";
                                System.out.println("IO_ERROR");
                                //  holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case DECODING_ERROR:
                                //message = "Image cannot be decoded...";
                                System.out.println("DECODING_ERROR");
                                //  holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case NETWORK_DENIED:
                                //message = "Downloads are denied";
                                System.out.println("NETWORK_DENIED");
                                // holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case OUT_OF_MEMORY:
                                //message = "Out Of Memory error";
                                System.out.println("OUT_OF_MEMORY");
                                //  holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case UNKNOWN:
                                //message = "Unknown error";
                                System.out.println("UNKNOWN");
                                // holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                        }
                        download.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        //super.onLoadingComplete(imageUri, view, loadedImage);
                        download.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        super.onLoadingCancelled(imageUri, view);
                    }
                });


                if (user_id.equals(snazl_id)){
                    btn_delete.setVisibility(View.VISIBLE);
                    btn_report.setVisibility(View.GONE);
                }else {
                    btn_delete.setVisibility(View.GONE);
                    btn_report.setVisibility(View.VISIBLE);
                }

                tv_snazl_text.setText(erp.getSnazl_title());

                iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogEdit.dismiss();
                    }
                });

                if (erp.getIsfav().equals("1")){
                    cb_favouritenew.setImageResource(R.drawable.fav_orange);
                    isFavnew = false;
                }else {
                    cb_favouritenew.setImageResource(R.drawable.fav);
                    isFavnew = true;
                }

                if (erp.getIslike().equals("1")){
                    cb_likenew.setImageResource(R.drawable.ic_liked);
                    isLikednew = false;
                }else {
                    cb_likenew.setImageResource(R.drawable.ic_thumb);
                    isLikednew = true;
                }

                cb_favouritenew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isFavnew) {
                            favToast = "Favorite";
                            cb_favouritenew.setImageResource(R.drawable.fav_orange);
                             new AddRemoveFavouriteExplore().execute();
                            isFavnew = false;
                            erp.setIsfav("1");
                        } else {
                            favToast = "Favorite Removed";
                            cb_favouritenew.setImageResource(R.drawable.fav);
                             new AddRemoveFavouriteExplore().execute();
                            isFavnew = true;
                            erp.setIsfav("0");
                        }

                    }
                });

                cb_likenew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isLikednew) {
                            cb_likenew.setImageResource(R.drawable.ic_liked);
                            isLikednew = false;
                            erp.setIslike("1");
                        } else {
                            cb_likenew.setImageResource(R.drawable.ic_thumb);
                            isLikednew = true;
                            erp.setIslike("0");
                        }

                        new AddRemoveLikeExplore().execute();
                    }
                });

                cb_share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Bitmap bitmap=((BitmapDrawable)iv_snazl_invisible.getDrawable()).getBitmap();

                        String pathofBmp = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap,"Snazy Image", null);
                        Uri bmpUri = Uri.parse(pathofBmp);
                        final Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                        shareIntent.setType("image/*");
                        startActivity(Intent.createChooser(shareIntent, "Share image using"));

                    }
                });

                btn_report.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                         dialogreport = new Dialog(getActivity());
                        dialogreport.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogreport.setContentView(R.layout.report_send_dialog);
                        dialogreport.getWindow().setBackgroundDrawableResource(R.color.transparent);

                        Window window = dialogreport.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();
//                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        wlp.gravity = Gravity.CENTER;

                        window.setAttributes(wlp);

                        final ImageView iv_close_report = (ImageView) dialogreport.findViewById(R.id.iv_closereport);
                        final Button btn_sendreport = (Button) dialogreport.findViewById(R.id.btn_sendreport);
                        final EditText et_messagereport = (EditText) dialogreport.findViewById(R.id.et_messagereport);

                        iv_close_report.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                dialogreport.dismiss();
                            }
                        });


                        btn_sendreport.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                report_message=et_messagereport.getText().toString().trim();

                                if (report_message.isEmpty()){

                                    Toast.makeText(getActivity(),"Enter message",Toast.LENGTH_SHORT).show();

                                }else {


                                    new SendRepotExplore().execute();
                                   // Toast.makeText(getActivity(),"s_id : "+snazl_id+"  u_id : "+snazl_user_id,Toast.LENGTH_SHORT).show();



                                }
                            }
                        });

                        dialogreport.show();


                    }
                });


                tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), UserProfile.class);
                        intent.putExtra("snazl_user_id",snazl_user_id);
                        startActivity(intent);
                    }
                });

                dialogEdit.show();
            }
        });
    }

    public class AddRemoveFavouriteExplore extends AsyncTask<String, Void, Void>{

        String status,message;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {

                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("user_id",user_id));
                    nameValuePairs.add(new BasicNameValuePair("snazl_id", snazl_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"fav_unfav.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");
                    message = js.getString("message");

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;

            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(getActivity(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){
                    if (message.equalsIgnoreCase("add favorite snazl")) {
                        Toast toast = Toast.makeText(getActivity(), "Favorite", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }else {
                        Toast toast = Toast.makeText(getActivity(), "UnFavorite", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }else {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }

    public class AddRemoveLikeExplore extends AsyncTask<String, Void, Void>{

        String status,message;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {

                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("user_id",user_id));
                    nameValuePairs.add(new BasicNameValuePair("snazl_id", snazl_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"addlike.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");
                    message = js.getString("message");

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;

            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(getActivity(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){
                    switch (message) {
                        case "Unlike Snazl": {
                            Toast toast = Toast.makeText(getActivity(), "Unliked", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            break;
                        }
                        case "like snazl": {
                            Toast toast = Toast.makeText(getActivity(), "Liked", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            break;
                        }
                        default: {
                            Toast toast = Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            break;
                        }
                    }
                }else {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }

    /* ****************************  my snazal ************************* */
    public class MySnazzyOperation extends AsyncTask<String, Void, Void> {

        String status, message;

        @Override
        protected void onPreExecute() {
            progressDialog_new.show();
            arralist_explore_snazzy_pojos.clear();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("user_id", user_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"snazzy.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");

                    if (status.equals("Success")){
                        JSONArray snazl_detail = js.getJSONArray("snazlDetal");
                        for (int i=0 ; i<snazl_detail.length() ; i++){

                            JSONObject jo = snazl_detail.getJSONObject(i);
                            String snazl_id = jo.getString("snazl_id");
                            String snazl_title = jo.getString("snazl_title");
                         //   String follower_follower_id = jo.getString("follower_follower_id");
                         //   String user_name = jo.getString("user_name");
                            String isfav = jo.getString("isfav");
                            String snazl_category_id = jo.getString("snazl_category_id");
                            String snazl_user_id = jo.getString("snazl_user_id");
                            String snazl_weblink = jo.getString("snazl_weblink");
                            String snazl_bg_type = jo.getString("snazl_bg_type");
                            String snazl_bg_color = jo.getString("snazl_bg_color");
                            String snazl_bg_image = jo.getString("snazl_bg_image");
                            String snazl_bg_x = jo.getString("snazl_bg_x");
                            String snazl_bg_y = jo.getString("snazl_bg_y");
                            String snazl_pinch_ratio = jo.getString("snazl_pinch_ratio");
                            String snazl_isPrivate = jo.getString("snazl_isPrivate");
                            String islike = jo.getString("islike");
                            String snazl_url = jo.getString("snazel_image_url");

                            String user_name="";           // no use
                            String follower_follower_id=""; // no use

                            if (snazl_isPrivate.equals("0")){
                                esp = new Explore_Snazzy_Pojo(snazl_id,snazl_title,snazl_category_id,snazl_user_id,snazl_weblink,snazl_bg_type,
                                        snazl_bg_color,snazl_bg_image,snazl_bg_x,snazl_bg_y,snazl_pinch_ratio,snazl_isPrivate,follower_follower_id,
                                        user_name,islike,isfav,snazl_url);

                                arralist_explore_snazzy_pojos.add(esp);

                            }



                            /*String snazl_title = jo.getString("snazl_title");
                            String snazel_image_url = jo.getString("snazel_image_url");

                            snazzy_arraylist.add(snazl_title);
                            snazzy_image_arraylist.add(snazel_image_url);
*/
                        }
                    }else if (status.equals("Fail")){
                        message = js.getString("message");
                    }

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;
            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog_new.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(getActivity(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){

                    snazzyAdapter = new SnazzyAdapter(getActivity(),arralist_explore_snazzy_pojos);
                    listSnazzy.setAdapter(snazzyAdapter);


                }else {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }


    /* ****************************  my snazal recent************************* */
    public class MySnazzyRecentOperation extends AsyncTask<String, Void, Void> {

        String status, message;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            arralist_explore_recent_pojos.clear();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("user_id", user_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"recentaddedsnzl.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");

                    if (status.equals("Success")){
                        JSONArray snazl_detail = js.getJSONArray("snazlDetal");
                        for (int i=0 ; i<snazl_detail.length() ; i++){
                            JSONObject jo = snazl_detail.getJSONObject(i);
                            String snazl_id = jo.getString("snazl_id");
                            String snazl_title = jo.getString("snazl_title");
                            //   String follower_follower_id = jo.getString("follower_follower_id");
                            //   String user_name = jo.getString("user_name");
                            String isfav = jo.getString("isfav");
                            String snazl_category_id = jo.getString("snazl_category_id");
                            String snazl_user_id = jo.getString("snazl_user_id");
                            String snazl_weblink = jo.getString("snazl_weblink");
                            String snazl_bg_type = jo.getString("snazl_bg_type");
                            String snazl_bg_color = jo.getString("snazl_bg_color");
                            String snazl_bg_image = jo.getString("snazl_bg_image");
                            String snazl_bg_x = jo.getString("snazl_bg_x");
                            String snazl_bg_y = jo.getString("snazl_bg_y");
                            String snazl_pinch_ratio = jo.getString("snazl_pinch_ratio");
                            String snazl_isPrivate = jo.getString("snazl_isPrivate");
                            String islike = jo.getString("islike");
                            String snazl_url = jo.getString("snazel_image_url");

                            String user_name="";           // no use
                            String follower_follower_id=""; // no use

                            if (snazl_isPrivate.equals("0")){

                                erp = new Explore_Recent_Pojo(snazl_id,snazl_title,snazl_category_id,snazl_user_id,snazl_weblink,snazl_bg_type,
                                        snazl_bg_color,snazl_bg_image,snazl_bg_x,snazl_bg_y,snazl_pinch_ratio,snazl_isPrivate,follower_follower_id,
                                        user_name,islike,isfav,snazl_url);

                                arralist_explore_recent_pojos.add(erp);

                            }



                        }
                    }else if (status.equals("Fail")){
                        message = js.getString("message");
                    }

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;
            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(getActivity(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){

                    mySnazal_Recent_Adapter = new MySnazal_Recent_Adapter(getActivity(),arralist_explore_recent_pojos);
                    gridview.setAdapter(mySnazal_Recent_Adapter);

                }else {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }

    public class SendRepotExplore extends AsyncTask<String, Void, Void>{

        String status,message;

        @Override
        protected void onPreExecute() {
            progressDialog_report.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {

                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("user_id",snazl_user_id));
                    nameValuePairs.add(new BasicNameValuePair("snazl_id", snazl_id));
                    nameValuePairs.add(new BasicNameValuePair("message", report_message));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"addreport.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");
                    message = js.getString("message");

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;

            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            progressDialog_report.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(getActivity(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){

                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    report_message="";
                    dialogreport.dismiss();



                }else {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }


    @Override
    public void onPause() {
        super.onPause();

        progressDialog_new.dismiss();
        progressDialog.dismiss();
        progressDialog_report.dismiss();
    }
}
