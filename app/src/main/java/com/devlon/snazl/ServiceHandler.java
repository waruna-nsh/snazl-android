package com.devlon.snazl;

import android.util.Log;

import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class ServiceHandler {

	private static final String IMGUR_CLIENT_ID = "...";
	private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/jpeg");
	private static final MediaType MEDIA_TYPE_FILE = MediaType.parse("multipart/form-data");

	public static final MediaType JSON
			= MediaType.parse("application/json; charset=utf-8");

	static String response = null;
	public final static int GET = 1;
	public final static int POST = 2;

	String s=null;
	public String url = null;
	public String param = null;
	public String filepath = null;
	public String requestString=null;
	public String url1 = null;
	public String param2 = null;
	public String filepath1 = null;
	public String requestString1=null;
	public String imageName=null;

	public String url_file = null;
	public String param_file = null;
	public String filepath_file = null;
	public String requestString_file=null;
	public String fileName=null;
	public String fileType=null;

	public File fileobject = null;

	public String url_file1 = null;
	public String param_file1 = null;
	public String filepath_file1 = null;
	public String requestString_file1=null;
	public String fileName1=null;

	public File fileobject1 = null;

	public String url3 = null;
	public String param3 = null;
	public String filepath3 = null;
	public String requestString3=null;

	OkHttpClient client = new OkHttpClient();

	String doGetRequest(String url) throws IOException
	{
		Request request = new Request.Builder().url(url).build();

		Response response = client.newCall(request).execute();
		return response.body().string();
	}

	public ServiceHandler() {

	}

	/*
	 * Making service call
	 * @url - url to make request
	 * @method - http request method
	 * */
	public String makeServiceCall(String url, int method) {
		return this.makeServiceCall(url, method, null);
	}

	/*
	 * Making service call
	 * @url - url to make request
	 * @method - http request method
	 * @params - http request params
	 * */
	public String makeServiceCall(String url, int method,
			List<NameValuePair> params) {
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;
			
			// Checking http request method type
			if (method == POST) {
				Log.e("in POST", "in POST");
				HttpPost httpPost = new HttpPost(url);
				// adding post params
				if (params != null) {
					Log.e("in POST params", "in POST params");
					httpPost.setEntity(new UrlEncodedFormEntity(params));
				}
				Log.e("url in post service", url);
				httpResponse = httpClient.execute(httpPost);

			} else if (method == GET) {
				// appending params to url
				Log.e("in GET", "in GET");
				if (params != null) {
					Log.e("in GET params", "in GET params");
					String paramString = URLEncodedUtils
							.format(params, "utf-8");
					url += "?" + paramString;
				}
				Log.e("url in get service", url);
				HttpGet httpGet = new HttpGet(url);

				httpResponse = httpClient.execute(httpGet);

			}
			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return response;

	}

	public Response run() throws Exception
	{
		Log.e("img", "image" + filepath);

		client.setReadTimeout(6000, TimeUnit.SECONDS);
		// Use the imgur image upload API as documented at https://api.imgur.com/endpoints/image
		RequestBody requestBody = new MultipartBuilder()
				.type(MultipartBuilder.FORM)
				.addPart(
						Headers.of("Content-Disposition", "form-data; name=\"image\";filename=\"10566design-1.jpg\""),
						RequestBody.create(MEDIA_TYPE_PNG, new File(filepath)) )
						//Headers.of("Content-Disposition", "form-data; name=\"status_image\""),
						//RequestBody.create(MEDIA_TYPE_PNG, new File(filepath)))
				.build();

		Request request = new Request.Builder()
				.url(url)
				.post(requestBody)
				.build();

		Response response = client.newCall(request).execute();
		if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
		Log.e("", " res prior : " + response.priorResponse());
		Log.e("", " res prior: " + response.networkResponse().toString());
		return  response;
	}

	public  Response uploadImage(String tempURL,String tempFilePath,String param1,String tempRequestString) throws Exception
	{

		url      =  tempURL;
		filepath =  tempFilePath;
		param    =  param1;
		requestString= tempRequestString;

		Response serviceresponse = run();

		return serviceresponse;

	}

	public Response run1() throws Exception
	{
		Log.e("img", "image" + filepath1);

		client.setReadTimeout(6000, TimeUnit.SECONDS);
		// Use the imgur image upload API as documented at https://api.imgur.com/endpoints/image
		RequestBody requestBody = new MultipartBuilder()
				.type(MultipartBuilder.FORM)
				.addPart(
						Headers.of("Content-Disposition", "form-data; name=\"" + param2 + "\";filename=\""+imageName+"\""),
						RequestBody.create(MEDIA_TYPE_PNG, new File(filepath1)))
						//Headers.of("Content-Disposition", "form-data; name=\"status_image\""),
						//RequestBody.create(MEDIA_TYPE_PNG, new File(filepath)))
				.build();

		Request request = new Request.Builder()
				.url(url1)
				.post(requestBody)
				.build();

		Response response = client.newCall(request).execute();
		if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
		Log.e("", " res prior : " + response.priorResponse());
		Log.e("", " res prior: " + response.networkResponse().toString());
		return  response;
	}

	public  Response uploadImageFile(String tempURL,String tempFilePath,String param3,String tempRequestString, String tempName) throws Exception
	{
		url1      =  tempURL;
		filepath1 =  tempFilePath;
		param2   =  param3;
		requestString1= tempRequestString;
		imageName = tempName;

		Response serviceresponse = run1();

		return serviceresponse;

	}

	public Response run2() throws Exception
	{
		Log.e("img", "image" + filepath_file);

		client.setReadTimeout(6000, TimeUnit.SECONDS);
		// Use the imgur image upload API as documented at https://api.imgur.com/endpoints/image
		RequestBody requestBody = new MultipartBuilder()
				.type(MultipartBuilder.FORM)
				.addPart(
						Headers.of("Content-Disposition", "form-data; name=\"" + fileType + "\";filename=\"" + fileName + "\""),
						RequestBody.create(MEDIA_TYPE_FILE, fileobject))
						//Headers.of("Content-Disposition", "form-data; name=\"status_image\""),
						//RequestBody.create(MEDIA_TYPE_PNG, new File(filepath)))
				.build();

		Request request = new Request.Builder()
				.url(url_file)
				.post(requestBody)
				.build();

		Response response = client.newCall(request).execute();

		if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
		return  response;
	}

	public  Response uploadFile(String tempURL,String tempFilePath,String param1,String tempRequestString, String tempFileName, String temptype,File fileobject) throws Exception
	{

		this.fileobject = fileobject;
		url_file      =  tempURL;
		filepath_file =  tempFilePath;
		param_file    =  param1;
		requestString_file= tempRequestString;
		fileName = tempFileName;
		fileType = temptype;

		Response serviceresponse = run2();

		return serviceresponse;

	}

	public Response runFile() throws Exception
	{
		Log.e("img", "image" + filepath_file1);

		client.setReadTimeout(6000, TimeUnit.SECONDS);
		// Use the imgur image upload API as documented at https://api.imgur.com/endpoints/image
		RequestBody requestBody = new MultipartBuilder()
				.type(MultipartBuilder.FORM)
				.addPart(
						Headers.of("Content-Disposition", "form-data; name=\"file_name\";filename=\"" + fileName1 + "\""),
						RequestBody.create(MEDIA_TYPE_FILE, fileobject1))
						//Headers.of("Content-Disposition", "form-data; name=\"status_image\""),
						//RequestBody.create(MEDIA_TYPE_PNG, new File(filepath)))
				.build();

		Request request = new Request.Builder()
				.url(url_file1)
				.post(requestBody)
				.build();

		Response response = client.newCall(request).execute();

		if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
		return  response;
	}

	public  Response uploadFileManager(String tempURL,String tempFilePath,String param1,String tempRequestString, String tempFileName,File fileobject) throws Exception
	{

		this.fileobject1 = fileobject;
		url_file1      =  tempURL;
		filepath_file1 =  tempFilePath;
		param_file1    =  param1;
		requestString_file1= tempRequestString;
		fileName1 = tempFileName;

		Response serviceresponse = runFile();

		return serviceresponse;

	}

	public Response run3() throws Exception
	{
		Log.e("img", "image" + filepath3);

		client.setReadTimeout(6000, TimeUnit.SECONDS);
		// Use the imgur image upload API as documented at https://api.imgur.com/endpoints/image
		RequestBody requestBody = new MultipartBuilder()
				.type(MultipartBuilder.FORM)
				.addPart(
						Headers.of("Content-Disposition", "form-data; name=\"companyLogo\";filename=\"10566design-1.jpg\""),
						RequestBody.create(MEDIA_TYPE_PNG, new File(filepath3)) )
						//Headers.of("Content-Disposition", "form-data; name=\"status_image\""),
						//RequestBody.create(MEDIA_TYPE_PNG, new File(filepath)))
				.build();

		Request request = new Request.Builder()
				.url(url3)
				.post(requestBody)
				.build();

		Response response = client.newCall(request).execute();
		if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
		Log.e("", " res prior : " + response.priorResponse());
		Log.e("", " res prior: " + response.networkResponse().toString());
		return  response;
	}

	public  Response uploadCompanyImage(String tempURL,String tempFilePath,String param1,String tempRequestString) throws Exception
	{

		url3      =  tempURL;
		filepath3 =  tempFilePath;
		param3   =  param1;
		requestString3 = tempRequestString;

		Response serviceresponse = run3();

		return serviceresponse;

	}

	public String getJsonString(String url1,List<NameValuePair> urls)
	{
		//String url1=null;
		if (urls != null) {
			Log.e("in GET params", "in GET params");
			String paramString = URLEncodedUtils.format(urls, "utf-8");
			url1 += "?" + paramString;
		}
       /* else
        {
            try
            {
                Log.e("","URL 1 : " + url1);
                s = doGetRequest(url1);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }*/

		try
		{
			Log.e("", "URL 1 : " + url1);
			s = doGetRequest(url1);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return s;
	}

	public String makeServiceCallIMAGE(String url, int method,
									   List<NameValuePair> params) {
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			// Checking http request method type
			if (method == POST) {
				HttpPost httpPost = new HttpPost(url);
				// adding post params
				if (params != null) {
					httpPost.setEntity(new UrlEncodedFormEntity(params));

				}

				httpResponse = httpClient.execute(httpPost);

			} else if (method == GET) {
				// appending params to url
				if (params != null) {
					String paramString = URLEncodedUtils
							.format(params, "utf-8");
					url += "?" + paramString;
				}
				HttpGet httpGet = new HttpGet(url);

				httpResponse = httpClient.execute(httpGet);

			}
			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return response;

	}
}
