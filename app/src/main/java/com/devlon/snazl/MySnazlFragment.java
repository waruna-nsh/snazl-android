package com.devlon.snazl;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import at.markushi.ui.CircleButton;

/**
 * Created by User on 3/15/2016.
 */
public class MySnazlFragment extends Fragment {

    GridView grid_mySnazl;
    ImageView btnAddSnazl, iv_search;
    ArrayList<SnazlObjects> textValueLists = new ArrayList<SnazlObjects>();
//    ArrayList<String> snazlTextLists = new ArrayList<String>();
    SnazlAdapter adapter;
    EditText et_search;
    String search_snazl, favToast;
    Boolean isFav = true, isLiked = true;
    NetworkConnection nw;
    ProgressDialog progressDialog;
    ProgressDialog progressDialog_report;
    Boolean netConnection = false;
    CircleButton cb_favourite,cb_like;
    ArrayList<SnazlPojo> mySnazlLists = new ArrayList<>();
    SnazlPojo sp;

    SessionManager sm;
    String user_id, snazl_id;
    String report_message;

    TextView tv_nosnazl;

    ImageLoader imageLoader;
    DisplayImageOptions options;
    private long mLastClickTime = 0;
     Dialog dialogreport;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mySnazlLists.clear();
        return inflater.inflate(R.layout.my_snazl_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        final int position = FragmentPagerItem.getPosition(getArguments());

        nw = new NetworkConnection(getActivity());
        sm = new SessionManager(getActivity());

        HashMap<String,String> userDetails = sm.getUserDetails();
        user_id = userDetails.get(SessionManager.KEY_ID);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        progressDialog_report = new ProgressDialog(getActivity());
        progressDialog_report.setMessage("Please Wait...");
        progressDialog_report.setCancelable(false);


        grid_mySnazl = (GridView) view.findViewById(R.id.grid_mySnazl);
        btnAddSnazl = (ImageView) view.findViewById(R.id.btnAddSnazl);
        iv_search = (ImageView) view.findViewById(R.id.iv_search);
        et_search = (EditText) view.findViewById(R.id.et_search);
        tv_nosnazl = (TextView) view.findViewById(R.id.tv_nosnazl);

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                search_snazl = et_search.getText().toString().trim();
                if (search_snazl.isEmpty()) {
                    iv_search.setVisibility(View.VISIBLE);
                } else {
                    iv_search.setVisibility(View.GONE);
                }
            }
        });

        btnAddSnazl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AddSnazlActivity.class);
                startActivity(intent);
            }
        });

        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_search.setCursorVisible(true);
            }
        });

        grid_mySnazl.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();


                sp = mySnazlLists.get(i);

                snazl_id = sp.getSnazl_id();

                final Dialog dialogEdit = new Dialog(getActivity());
                dialogEdit.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialogEdit.setContentView(R.layout.open_snazl_dialog);
                dialogEdit.getWindow().setBackgroundDrawableResource(R.color.transparent);

                Window window = dialogEdit.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
//                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                wlp.gravity = Gravity.CENTER;

                window.setAttributes(wlp);

                final ImageView iv_close = (ImageView) dialogEdit.findViewById(R.id.iv_close);
                final ImageView iv_snazl_invisible = (ImageView) dialogEdit.findViewById(R.id.iv_snazl_invisible);
                cb_favourite = (CircleButton) dialogEdit.findViewById(R.id.cb_favourite);
                cb_like = (CircleButton) dialogEdit.findViewById(R.id.cb_like);
                CircleButton cb_share = (CircleButton) dialogEdit.findViewById(R.id.cb_share);
                final TextView tv_snazl_text = (TextView) dialogEdit.findViewById(R.id.tv_snazl_text);
                final TextView tv_username = (TextView) dialogEdit.findViewById(R.id.tv_username);
                final Button btn_delete = (Button) dialogEdit.findViewById(R.id.btn_delete);
                final Button btn_report = (Button) dialogEdit.findViewById(R.id.btn_report);
                final ProgressBar download = (ProgressBar) dialogEdit.findViewById(R.id.download);

                DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                        .cacheOnDisc(true).cacheInMemory(true)
                        .imageScaleType(ImageScaleType.EXACTLY)
                        .displayer(new FadeInBitmapDisplayer(300)).build();

                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                        .defaultDisplayImageOptions(defaultOptions)
                        .memoryCache(new WeakMemoryCache())
                        .discCacheSize(100 * 1024 * 1024).build();
                ImageLoader.getInstance().init(config);

                imageLoader = ImageLoader.getInstance();
                options = new DisplayImageOptions.Builder()
                        .cacheInMemory(true)
                        .cacheOnDisc(true)
                        .resetViewBeforeLoading(true).build();

                imageLoader.getInstance().displayImage(sp.getSnazl_url(), iv_snazl_invisible, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        download.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        String message = null;
                        switch (failReason.getType()) {
                            case IO_ERROR:
                                //message = "Input/Output error";
                                System.out.println("IO_ERROR");
                                //  holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case DECODING_ERROR:
                                //message = "Image cannot be decoded...";
                                System.out.println("DECODING_ERROR");
                                //  holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case NETWORK_DENIED:
                                //message = "Downloads are denied";
                                System.out.println("NETWORK_DENIED");
                                // holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case OUT_OF_MEMORY:
                                //message = "Out Of Memory error";
                                System.out.println("OUT_OF_MEMORY");
                                //  holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                            case UNKNOWN:
                                //message = "Unknown error";
                                System.out.println("UNKNOWN");
                                // holder.img_Product.setImageResource(R.drawable.no_image);
                                break;
                        }
                        download.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        //super.onLoadingComplete(imageUri, view, loadedImage);
                        download.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        super.onLoadingCancelled(imageUri, view);
                    }
                });



                if (user_id.equals(snazl_id)){
                    btn_delete.setVisibility(View.VISIBLE);
                    btn_report.setVisibility(View.GONE);
                }else {
                    btn_delete.setVisibility(View.GONE);
                    btn_report.setVisibility(View.VISIBLE);
                }

                tv_snazl_text.setText(sp.getSnazl_title());

                iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogEdit.dismiss();
                    }
                });

                if (sp.getIsfav().equals("1")){
                    cb_favourite.setImageResource(R.drawable.fav_orange);
                    isFav = false;
                }else {
                    cb_favourite.setImageResource(R.drawable.fav);
                    isFav = true;
                }

                if (sp.getIslike().equals("1")){
                    cb_like.setImageResource(R.drawable.ic_liked);
                    isLiked = false;
                }else {
                    cb_like.setImageResource(R.drawable.ic_thumb);
                    isLiked = true;
                }

                cb_favourite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isFav) {
                            favToast = "Favorite";
                            cb_favourite.setImageResource(R.drawable.fav_orange);
                            new AddRemoveFavourite().execute();
                            isFav = false;
                            sp.setIsfav("1");
                        } else {
                            favToast = "Favorite Removed";
                            cb_favourite.setImageResource(R.drawable.fav);
                            new AddRemoveFavourite().execute();
                            isFav = true;
                            sp.setIsfav("0");
                        }

                    }
                });

                cb_like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isLiked) {
                            cb_like.setImageResource(R.drawable.ic_liked);
                            isLiked = false;
                            sp.setIslike("1");
                        } else {
                            cb_like.setImageResource(R.drawable.ic_thumb);
                            isLiked = true;
                            sp.setIslike("0");
                        }

                        new AddRemoveLike().execute();
                    }
                });

                tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), UserProfile.class);
                        intent.putExtra("snazl_user_id",sp.getSnazl_user_id());
                        startActivity(intent);
                    }
                });

                cb_share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                         Bitmap bitmap=((BitmapDrawable)iv_snazl_invisible.getDrawable()).getBitmap();

                        String pathofBmp = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap,"Snazy Image", null);
                        Uri bmpUri = Uri.parse(pathofBmp);
                        final Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                        shareIntent.setType("image/*");
                        startActivity(Intent.createChooser(shareIntent, "Share image using"));

                    }
                });

                btn_report.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                         dialogreport = new Dialog(getActivity());
                        dialogreport.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogreport.setContentView(R.layout.report_send_dialog);
                        dialogreport.getWindow().setBackgroundDrawableResource(R.color.transparent);

                        Window window = dialogreport.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();
//                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        wlp.gravity = Gravity.CENTER;

                        window.setAttributes(wlp);

                        final ImageView iv_close_report = (ImageView) dialogreport.findViewById(R.id.iv_closereport);
                        final Button btn_sendreport = (Button) dialogreport.findViewById(R.id.btn_sendreport);
                        final EditText et_messagereport = (EditText) dialogreport.findViewById(R.id.et_messagereport);

                        iv_close_report.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                dialogreport.dismiss();
                            }
                        });


                        btn_sendreport.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                report_message=et_messagereport.getText().toString().trim();

                                if (report_message.isEmpty()){

                                     Toast.makeText(getActivity(),"Enter message",Toast.LENGTH_SHORT).show();

                                }else {

                                     new SendRepotMySnazl().execute();

                                }
                            }
                        });

                        dialogreport.show();


                    }
                });

                dialogEdit.show();
            }
        });

        new MySnazlOperation().execute();

    }

    public class AddRemoveFavourite extends AsyncTask<String, Void, Void>{

        String status,message;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {

                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("user_id",user_id));
                    nameValuePairs.add(new BasicNameValuePair("snazl_id", snazl_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"fav_unfav.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");
                    message = js.getString("message");

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;

            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(getActivity(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){
                    if (message.equalsIgnoreCase("add favorite snazl")) {
                        Toast toast = Toast.makeText(getActivity(), "Favorite", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }else {
                        Toast toast = Toast.makeText(getActivity(), "UnFavorite", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }else {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }

    public class AddRemoveLike extends AsyncTask<String, Void, Void>{

        String status,message;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {

                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("user_id",user_id));
                    nameValuePairs.add(new BasicNameValuePair("snazl_id", snazl_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"addlike.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");
                    message = js.getString("message");

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;

            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(getActivity(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){
                    switch (message) {
                        case "Unlike Snazl": {
                            Toast toast = Toast.makeText(getActivity(), "Unliked", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            break;
                        }
                        case "like snazl": {
                            Toast toast = Toast.makeText(getActivity(), "Liked", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            break;
                        }
                        default: {
                            Toast toast = Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            break;
                        }
                    }
                }else {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }

    public class MySnazlOperation extends AsyncTask<String, Void, Void>{

        String status, message;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            mySnazlLists.clear();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("userid", user_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"snazllist.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");

                    if (status.equals("success")){
                        JSONArray snazl_detail = js.getJSONArray("snazl_detail");
                        for (int i=0 ; i<snazl_detail.length() ; i++){
                            JSONObject jo = snazl_detail.getJSONObject(i);
                            String snazl_id = jo.getString("snazl_id");
                            String snazl_title = jo.getString("snazl_title");
                            String follower_follower_id = jo.getString("follower_follower_id");
                            String user_name = jo.getString("user_name");
                            String isfav = jo.getString("isfav");
                            String snazl_category_id = jo.getString("snazl_category_id");
                            String snazl_user_id = jo.getString("snazl_user_id");
                            String snazl_weblink = jo.getString("snazl_weblink");
                            String snazl_bg_type = jo.getString("snazl_bg_type");
                            String snazl_bg_color = jo.getString("snazl_bg_color");
                            String snazl_bg_image = jo.getString("snazl_bg_image");
                            String snazl_bg_x = jo.getString("snazl_bg_x");
                            String snazl_bg_y = jo.getString("snazl_bg_y");
                            String snazl_pinch_ratio = jo.getString("snazl_pinch_ratio");
                            String snazl_isPrivate = jo.getString("snazl_isPrivate");
                            String islike = jo.getString("islike");
                            String snazl_url = jo.getString("snazl_url");

                            sp = new SnazlPojo(snazl_id,snazl_title,snazl_category_id,snazl_user_id,snazl_weblink,snazl_bg_type,
                                    snazl_bg_color,snazl_bg_image,snazl_bg_x,snazl_bg_y,snazl_pinch_ratio,snazl_isPrivate,follower_follower_id,
                                    user_name,islike,isfav,snazl_url);

                            mySnazlLists.add(sp);

                        }
                    }else if (status.equals("Fail")){
                        message = js.getString("message");
                    }

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;
            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(getActivity(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("success")){
                    if (mySnazlLists.isEmpty()){
                        tv_nosnazl.setVisibility(View.VISIBLE);
                        grid_mySnazl.setVisibility(View.GONE);
                      /*  Toast toast = Toast.makeText(getActivity(), "No Snazl Found.", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();*/
                    }else {
                        tv_nosnazl.setVisibility(View.GONE);
                        grid_mySnazl.setVisibility(View.VISIBLE);
                        adapter = new SnazlAdapter(getActivity(),mySnazlLists);
                        grid_mySnazl.setAdapter(adapter);
                    }

                }else {
                    tv_nosnazl.setVisibility(View.VISIBLE);
                    grid_mySnazl.setVisibility(View.GONE);
                   /* Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();*/
                }
            }

            super.onPostExecute(aVoid);
        }
    }

    public class SendRepotMySnazl extends AsyncTask<String, Void, Void>{

        String status,message;

        @Override
        protected void onPreExecute() {
            progressDialog_report.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {

                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("user_id",user_id));
                    nameValuePairs.add(new BasicNameValuePair("snazl_id", snazl_id));
                    nameValuePairs.add(new BasicNameValuePair("message", report_message));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"addreport.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");
                    message = js.getString("message");

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;

            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            progressDialog_report.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(getActivity(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){

                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    dialogreport.dismiss();


                }else {
                    Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }


}
