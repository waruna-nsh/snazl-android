package com.devlon.snazl;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by User on 4/15/2016.
 */
public class InterestAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> textInterestLists;

    public InterestAdapter(Context context, ArrayList<String> textInterestLists){
        this.context = context;
        this.textInterestLists = textInterestLists;
    }

    @Override
    public int getCount() {
        return textInterestLists.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder{
        ImageView iv_cancel;
        TextView tv_interest;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        final ViewHolder holder;
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.single_interest, null);
            holder = new ViewHolder();

            holder.tv_interest = (TextView) convertView.findViewById(R.id.tv_interest);
            holder.iv_cancel = (ImageView) convertView.findViewById(R.id.iv_cancel);

            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

//        SnazlObjects so = textValueLists.get(position);

        holder.tv_interest.setText(textInterestLists.get(position));
        holder.iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textInterestLists.remove(position);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
}
