package com.devlon.snazl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateUser {

	private static Pattern pattern;
    private static Matcher matcher;
    
    private static final String EMAIL_PATTERN ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+
    								"[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    

    public static boolean validate(String email) {
    	
    	pattern = Pattern.compile(EMAIL_PATTERN);
    	matcher = pattern.matcher(email);
    	return matcher.matches();
    }
    
    /**
     * Checks for Null String object
     * 
     * @param txt
     * @return true for not null and false for null String object
     */
    public static boolean isNotNull(String txt){
        return txt!=null && txt.trim().length()>0 ? true: false;
    }
}
