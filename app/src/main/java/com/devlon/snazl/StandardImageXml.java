package com.devlon.snazl;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.opengl.GLES10;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.polites.android.GestureImageView;

import java.io.IOException;

import javax.microedition.khronos.opengles.GL10;

import at.markushi.ui.CircleButton;

public class StandardImageXml extends Activity {

    GestureImageView image;
    CircleButton btnAdd;
    private static int RESULT_LOAD_IMAGE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_standard_image_xml);

        image = (GestureImageView) findViewById(R.id.image);
        btnAdd = (CircleButton) findViewById(R.id.cb_image);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Rect rectf = new Rect();
                image.getLocalVisibleRect(rectf);

                Log.d("WIDTH        :", String.valueOf(rectf.width()));
                Log.d("HEIGHT       :", String.valueOf(rectf.height()));
                Log.d("left         :", String.valueOf(rectf.left));
                Log.d("right        :", String.valueOf(rectf.right));
                Log.d("top          :", String.valueOf(rectf.top));
                Log.d("bottom       :", String.valueOf(rectf.bottom));

                int[] l = new int[2];
                image.getLocationOnScreen(l);
                int x = l[0];
                int y = l[1];
                int w = image.getWidth();
                int h = image.getHeight();

                Constant.iv_x = x;
                Constant.iv_y = y;
                Constant.iv_w = w;
                Constant.iv_h = h;
                Constant.ivBackground = image;

                System.out.println("Bounds : " + x + " , " + y + " , " + w + " , " + h);

                int[] maxTextureSize = new int[1];
                GLES10.glGetIntegerv(GL10.GL_MAX_TEXTURE_SIZE, maxTextureSize, 0);
                Log.i("glinfo", "Max texture size = " + maxTextureSize[0]);

                /*if (rx < x || rx > x + w || ry < y || ry > y + h) {
                    return false;
                }*/

            }
        }, 500);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            System.out.println("File uri : " + picturePath);
            cursor.close();


            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

            int b_height = bitmap.getHeight();
            int b_width = bitmap.getWidth();
            System.out.println("Scale : "+b_height+" , "+b_width);

            if (b_height==b_width){
                image.setScaleType(ImageView.ScaleType.FIT_XY);
            }else {
                image.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }

            if(bitmap.getHeight()>=2048||bitmap.getWidth()>=2048){
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(picturePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotationInDegrees = exifToDegrees(rotation);

                Matrix matrix = new Matrix();
                if (rotation != 0f) {matrix.preRotate(rotationInDegrees);}

                Bitmap adjustedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                image.setImageBitmap(adjustedBitmap);
            }else {
                image.setImageBitmap(bitmap);
            }

        }

        if(resultCode != RESULT_OK) return;
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
        return 0;
    }



}
