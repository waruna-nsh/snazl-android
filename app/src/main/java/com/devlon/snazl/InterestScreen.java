package com.devlon.snazl;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jess.ui.TwoWayGridView;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import at.markushi.ui.CircleButton;
import it.sephiroth.android.library.widget.AdapterView;
import it.sephiroth.android.library.widget.HListView;

public class InterestScreen extends AppCompatActivity {

    HListView listYours;
    EditText et_interests;
    String interests, user_id, search_text;
    ImageView iv_back,iv_search;
    TwoWayGridView gridview_others;
    YoursAdapter yoursAdapter;
    OthersAdapter othersAdapter;
    ArrayList<String> textInterestLists = new ArrayList<>();
    NetworkConnection nw;
    ProgressDialog progressDialog;
    Boolean netConnection = false;
    SessionManager sm;

    ArrayList<SnazlPojo> mySnazlListsinterst = new ArrayList<>();
    ArrayList<SnazlPojo> mySnazlListsinterstOthers = new ArrayList<>();
    SnazlPojo sp;
    TextView tv_nosnazl;
    TextView tv_nosnazlgrid;
  String snazl_id;
    Boolean isFav = true, isLiked = true;
    CircleButton cb_favourite,cb_like;
    String search_snazl, favToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_screen);

        et_interests = (EditText) findViewById(R.id.et_interests);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        listYours = (HListView) findViewById(R.id.listYours);
        gridview_others = (TwoWayGridView) findViewById(R.id.gridview_others);
        tv_nosnazl = (TextView) findViewById(R.id.tv_nosnazl);
        tv_nosnazlgrid = (TextView) findViewById(R.id.tv_nosnazlgrid);

        mySnazlListsinterst=new ArrayList<SnazlPojo>();
        mySnazlListsinterstOthers=new ArrayList<SnazlPojo>();

        interests = getIntent().getStringExtra("interests");
        et_interests.setText(interests);

        nw = new NetworkConnection(InterestScreen.this);
        sm = new SessionManager(InterestScreen.this);

        HashMap<String,String> userDetails = sm.getUserDetails();
        user_id = userDetails.get(SessionManager.KEY_ID);

        progressDialog = new ProgressDialog(InterestScreen.this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        textInterestLists.add("movies");
        textInterestLists.add("concerts");
        textInterestLists.add("sports");
        textInterestLists.add("drama");
        textInterestLists.add("cultural");
        textInterestLists.add("cricket");
        textInterestLists.add("hollywood");


      //  gridview_others.setAdapter(yoursAdapter);
        new MySnazlOperationInterst().execute();

        iv_search = (ImageView) findViewById(R.id.iv_search);

        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_text = et_interests.getText().toString().trim();
                if (search_text.isEmpty()){
                    Toast toast = Toast.makeText(InterestScreen.this, "Please enter some text value to search.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }else {
                    new SearchOperation().execute();
                }
            }
        });

    }

    public class MySnazlOperationInterst extends AsyncTask<String, Void, Void>{

        String status, message;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            mySnazlListsinterst.clear();
            mySnazlListsinterstOthers.clear();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("userid", user_id));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"snazllist.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ",response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");

                    if (status.equals("success")){
                        JSONArray snazl_detail = js.getJSONArray("snazl_detail");
                        for (int i=0 ; i<snazl_detail.length() ; i++){
                            JSONObject jo = snazl_detail.getJSONObject(i);
                            String snazl_id = jo.getString("snazl_id");
                            String snazl_title = jo.getString("snazl_title");
                            String snazl_category_id = jo.getString("snazl_category_id");
                            String snazl_user_id = jo.getString("snazl_user_id");
                            String snazl_weblink = jo.getString("snazl_weblink");
                            String snazl_bg_type = jo.getString("snazl_bg_type");
                            String snazl_bg_color = jo.getString("snazl_bg_color");
                            String snazl_bg_image = jo.getString("snazl_bg_image");
                            String snazl_bg_x = jo.getString("snazl_bg_x");
                            String snazl_bg_y = jo.getString("snazl_bg_y");
                            String snazl_pinch_ratio = jo.getString("snazl_pinch_ratio");
                            String snazl_isPrivate = jo.getString("snazl_isPrivate");
                            String follower_follower_id = jo.getString("follower_follower_id");
                            String user_name = jo.getString("user_name");
                            String islike = jo.getString("islike");
                            String isfav = jo.getString("isfav");
                            String snazl_url = jo.getString("snazl_url");

                            sp = new SnazlPojo(snazl_id,snazl_title,snazl_category_id,snazl_user_id,snazl_weblink,snazl_bg_type,
                                    snazl_bg_color,snazl_bg_image,snazl_bg_x,snazl_bg_y,snazl_pinch_ratio,snazl_isPrivate,follower_follower_id,
                                    user_name,islike,isfav,snazl_url);

                             if (follower_follower_id.equals("null")){

                                 mySnazlListsinterst.add(sp);


                             }else {

                                 mySnazlListsinterstOthers.add(sp);
                             }


                        }
                    }else if (status.equals("Fail")){
                        message = js.getString("message");
                    }

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;
            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(InterestScreen.this, "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("success")){
                    if (mySnazlListsinterst.isEmpty()){
                        tv_nosnazl.setVisibility(View.VISIBLE);
                        listYours.setVisibility(View.GONE);
                      /*  Toast toast = Toast.makeText(getActivity(), "No Snazl Found.", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();*/
                    }else {

                        yoursAdapter = new YoursAdapter(InterestScreen.this,mySnazlListsinterst);
                        listYours.setAdapter(yoursAdapter);

                        tv_nosnazl.setVisibility(View.GONE);
                        listYours.setVisibility(View.VISIBLE);

                    }

                    if (mySnazlListsinterstOthers.isEmpty()){
                        tv_nosnazlgrid.setVisibility(View.VISIBLE);
                        gridview_others.setVisibility(View.GONE);
                      /*  Toast toast = Toast.makeText(getActivity(), "No Snazl Found.", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();*/
                    }else {

                        othersAdapter = new OthersAdapter(InterestScreen.this,mySnazlListsinterstOthers);
                        gridview_others.setAdapter(othersAdapter);

                        tv_nosnazlgrid.setVisibility(View.GONE);
                        gridview_others.setVisibility(View.VISIBLE);

                    }

                }else {
                    tv_nosnazl.setVisibility(View.VISIBLE);
                    tv_nosnazlgrid.setVisibility(View.VISIBLE);
                    listYours.setVisibility(View.GONE);
                    gridview_others.setVisibility(View.GONE);
                   /* Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();*/
                }
            }

            super.onPostExecute(aVoid);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_interest_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SearchOperation extends AsyncTask<String, Void, Void> {

        String status, message;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("user_id", user_id));
                    nameValuePairs.add(new BasicNameValuePair("search_str",search_text));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.snazlUrl+"searchsnazl.php",ServiceHandler.GET,nameValuePairs);

                    Log.e("Response : ", response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");

                }catch (Exception ex){
                    ex.printStackTrace();
                }
                netConnection = true;
            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(InterestScreen.this, "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (status.equals("Success")){
                    Toast toast = Toast.makeText(InterestScreen.this, "Success", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }else {
                    Toast toast = Toast.makeText(InterestScreen.this, message, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

            super.onPostExecute(aVoid);
        }
    }

}
