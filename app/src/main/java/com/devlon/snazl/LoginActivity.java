package com.devlon.snazl;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends Activity {

//    ImageView iv_cancel;
    EditText et_email, et_password;
    Button btnSignIn;
    TextView tv_forgotPassword, tv_signUp;
    Typeface custom_font, font_login;
    ProgressDialog prgDialog;
    NetworkConnection nw;
    Boolean netConnection = false;
    String userEmail, userPassword;
    SessionManager sm;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();
        Firebase.setAndroidContext(this);

        nw = new NetworkConnection(LoginActivity.this);
        sm = new SessionManager(LoginActivity.this);

        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Please Wait...");
        prgDialog.setCancelable(false);


//        iv_cancel = (ImageView) findViewById(R.id.iv_cancel);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);

        tv_forgotPassword = (TextView) findViewById(R.id.tv_forgotPassword);
        tv_signUp = (TextView) findViewById(R.id.tv_signUp);

        custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        font_login = Typeface.createFromAsset(getAssets(), "fonts/ufonts.com_schulbuchnord-fett.ttf");

        tv_forgotPassword.setTypeface(custom_font);
        tv_signUp.setTypeface(custom_font);

        btnSignIn.setTypeface(custom_font);
        et_email.setTypeface(custom_font);
        et_email.setTransformationMethod(new SingleLineTransformationMethod());
        et_password.setTypeface(custom_font);
        et_password.setTransformationMethod(new PasswordTransformationMethod());

        /*iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });*/

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userEmail = et_email.getText().toString().trim();
                userPassword = et_password.getText().toString().trim();
                if (ValidateUser.isNotNull(userEmail) && ValidateUser.isNotNull(userPassword)) {
                    if (ValidateUser.validate(userEmail)) {

                        firebaseAuth.signInWithEmailAndPassword(userEmail, userPassword)
                                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (!task.isSuccessful()) {

                                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                            builder.setMessage(task.getException().getMessage())
                                                    .setTitle("Login Error")
                                                    .setPositiveButton(android.R.string.ok, null);
                                            AlertDialog dialog = builder.create();
                                            dialog.show();

                                        } else {

                                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);


                                            //ref.child("User").setValue(user);

                                            startActivity(intent);
                                            finish();
                                        }
                                    }
                                });

                        //new LoginOperation().execute();

                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(), "Please enter valid email address.", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Please fill entire form to proceed.", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });

        tv_forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,ForgotPassActivity.class);
                startActivity(intent);
                finish();
            }
        });


        tv_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,SignupActivity.class);
                startActivity(intent);
                finish();
            }
        });

        int density= getResources().getDisplayMetrics().densityDpi;

        switch(density)
        {

            case DisplayMetrics.DENSITY_LOW:
                System.out.println("Density : LDPI");
                break;

            case DisplayMetrics.DENSITY_MEDIUM:
                System.out.println("Density : MDPI");
                break;

            case DisplayMetrics.DENSITY_HIGH:
                System.out.println("Density : HDPI");
                break;

            case DisplayMetrics.DENSITY_XHIGH:
                System.out.println("Density : XHDPI");
                break;

            case DisplayMetrics.DENSITY_XXHIGH:
                System.out.println("Density : XXHDPI");
                break;

            default:
                System.out.println("Density : Default");

        }
    }

    public class LoginOperation extends AsyncTask<String, Void, Void> {

        String status, message, user_id, user_name, user_email_address;

        @Override
        protected void onPreExecute() {
            prgDialog.show();
        }


        @Override
        protected Void doInBackground(String... strings) {

            if (nw.isConnectingToInternet() == true) {
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    nameValuePairs.add(new BasicNameValuePair("method", "SignIn"));
                    nameValuePairs.add(new BasicNameValuePair("email", userEmail));
                    nameValuePairs.add(new BasicNameValuePair("password", userPassword));
//                    nameValuePairs.add(new BasicNameValuePair("reg_id", regID));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.apiUrl, ServiceHandler.GET,
                            nameValuePairs);

                    Log.e("response", response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");
                    if (status.contains("Fail")) {
                        message = js.getString("message");
                    }else if (status.equals("Success")){
                        JSONObject user_info = js.getJSONObject("user_info");
                        user_id = user_info.getString("user_id");
                        user_name = user_info.getString("user_name");
                        user_email_address = user_info.getString("user_email_address");
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                netConnection = true;
            } else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (netConnection == false) {
                Toast toast = Toast.makeText(getApplicationContext(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (status.contains("Success")) {

                    sm.UserSession(user_id,user_email_address,user_name);
                    sm.isLoggedIn();

                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
            super.onPostExecute(result);
        }

    }

}
