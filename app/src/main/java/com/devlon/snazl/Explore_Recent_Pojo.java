package com.devlon.snazl;

/**
 * Created by Devlon Android 2 on 05-Oct-16.
 */
public class Explore_Recent_Pojo {
    public String snazl_id, snazl_title, snazl_category_id, snazl_user_id, snazl_weblink, snazl_bg_type, snazl_bg_color,
            snazl_bg_image, snazl_bg_x, snazl_bg_y, snazl_pinch_ratio, snazl_isPrivate, follower_follower_id, user_name, islike, isfav;

    String snazl_url;

    public Explore_Recent_Pojo(String temp_snazl_id , String temp_snazl_title, String temp_snazl_category_id, String temp_snazl_user_id, String temp_snazl_weblink
            , String temp_snazl_bg_type, String temp_snazl_bg_color, String temp_snazl_bg_image, String temp_snazl_bg_x, String temp_snazl_bg_y
            , String temp_snazl_pinch_ratio, String temp_snazl_isPrivate, String temp_follower_follower_id, String temp_user_name, String temp_islike, String temp_isfav,
                               String snazl_url){

        this.snazl_id = temp_snazl_id;
        this.snazl_title = temp_snazl_title;
        this.snazl_category_id = temp_snazl_category_id;
        this.snazl_user_id = temp_snazl_user_id;
        this.snazl_weblink = temp_snazl_weblink;
        this.snazl_bg_type = temp_snazl_bg_type;
        this.snazl_bg_color = temp_snazl_bg_color;
        this.snazl_bg_image = temp_snazl_bg_image;
        this.snazl_bg_x = temp_snazl_bg_x;
        this.snazl_bg_y = temp_snazl_bg_y;
        this.snazl_pinch_ratio = temp_snazl_pinch_ratio;
        this.snazl_isPrivate = temp_snazl_isPrivate;
        this.follower_follower_id = temp_follower_follower_id;
        this.user_name = temp_user_name;
        this.islike = temp_islike;
        this.isfav = temp_isfav;
        this.snazl_url = snazl_url;

    }

    public String getSnazl_id() {
        return snazl_id;
    }

    public String getSnazl_title() {
        return snazl_title;
    }

    public String getSnazl_category_id() {
        return snazl_category_id;
    }

    public String getSnazl_user_id() {
        return snazl_user_id;
    }

    public String getSnazl_weblink() {
        return snazl_weblink;
    }

    public String getSnazl_bg_type() {
        return snazl_bg_type;
    }

    public String getSnazl_bg_color() {
        return snazl_bg_color;
    }

    public String getSnazl_bg_image() {
        return snazl_bg_image;
    }

    public String getSnazl_bg_x() {
        return snazl_bg_x;
    }

    public String getSnazl_bg_y() {
        return snazl_bg_y;
    }

    public String getSnazl_pinch_ratio() {
        return snazl_pinch_ratio;
    }

    public String getSnazl_isPrivate() {
        return snazl_isPrivate;
    }

    public String getFollower_follower_id() {
        return follower_follower_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getIslike() {
        return islike;
    }

    public String getIsfav() {
        return isfav;
    }

    public void setIslike(String islike) {
        this.islike = islike;
    }

    public void setIsfav(String isfav) {
        this.isfav = isfav;
    }

    public String getSnazl_url() {
        return snazl_url;
    }
}
