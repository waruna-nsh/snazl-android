package com.devlon.snazl;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by User on 4/15/2016.
 */
public class RecentAdapter extends BaseAdapter {

    Context context;
    ArrayList<SnazlObjects> textValueLists;
    ArrayList<String> snazlTextLists;

    public RecentAdapter(Context context, ArrayList<String> snazlTextLists){
        this.context = context;
        this.snazlTextLists = snazlTextLists;
    }

    @Override
    public int getCount() {
        return snazlTextLists.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder{
        ImageView iv_snazl_image;
        TextView tv_snazl_text;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        final ViewHolder holder;
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.single_recent, null);
            holder = new ViewHolder();

            holder.tv_snazl_text = (TextView) convertView.findViewById(R.id.tv_snazl_text);
            holder.iv_snazl_image = (ImageView) convertView.findViewById(R.id.iv_snazl_image);

            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

//        SnazlObjects so = textValueLists.get(position);

        holder.tv_snazl_text.setText(snazlTextLists.get(position));

        return convertView;
    }
}
