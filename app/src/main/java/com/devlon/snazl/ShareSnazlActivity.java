package com.devlon.snazl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Response;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import at.markushi.ui.CircleButton;

public class ShareSnazlActivity extends AppCompatActivity {
    Drawable myDrawable;
    TextView tv_links, toolbar_title;
    Button btnSendSms, btnPrivate, btnPublic;
    CircleButton cb_share, cb_download, cb_tag, cb_favourite;
    Boolean isFav = true;
    String linkUrl, user_id, isFavorite="0", isPrivate="1";
    Typeface custom_font;
    ImageView iv_mySnazl;

    NetworkConnection nw;
    ProgressDialog progressDialog;
    Boolean netConnection = false;
    SessionManager sm;
    Boolean noData = false;
    JSONArray array;

    String snazl_id;
    File file_key;
    Response response;
    String stnrulr;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_snazl);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setTitle("");
//        myToolbar.setTitle("Cancel");
        myToolbar.setTitleTextColor(Color.WHITE);

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            myDrawable = getApplicationContext().getDrawable(R.drawable.ic_overflow);
        } else {
            myDrawable = getApplicationContext().getResources().getDrawable(R.drawable.ic_overflow);
        }

        iv_mySnazl = (ImageView) findViewById(R.id.iv_mySnazl);
        iv_mySnazl.setImageBitmap(Constant.snazlBitmap);

        myToolbar.setOverflowIcon(myDrawable);
        custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");

        btnSendSms = (Button) findViewById(R.id.btnSendSms);
        btnPrivate = (Button) findViewById(R.id.btnPrivate);
        btnPublic = (Button) findViewById(R.id.btnPublic);
        cb_share = (CircleButton) findViewById(R.id.cb_share);
        cb_download = (CircleButton) findViewById(R.id.cb_download);
        cb_tag = (CircleButton) findViewById(R.id.cb_tag);
        cb_favourite = (CircleButton) findViewById(R.id.cb_favourite);
        tv_links = (TextView) findViewById(R.id.tv_links);

        nw = new NetworkConnection(ShareSnazlActivity.this);
        sm = new SessionManager(ShareSnazlActivity.this);

        HashMap<String,String> userDetails = sm.getUserDetails();
        user_id = userDetails.get(SessionManager.KEY_ID);

        progressDialog = new ProgressDialog(ShareSnazlActivity.this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);

        linkUrl = getIntent().getStringExtra("linkUrl");
        snazl_id = getIntent().getStringExtra("snazl_id");
        file_key=(File)getIntent().getExtras().get("file_key");

        System.out.println("share button : "+ linkUrl +"  "+snazl_id+" "+String.valueOf(file_key)+""+file_key.getName());

        if (linkUrl.isEmpty()) {

            tv_links.setText("No Url");

        }else {
            tv_links.setText(linkUrl);
        }

        toolbar_title.setTypeface(custom_font);
        toolbar_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        cb_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFav){
                    cb_favourite.setImageResource(R.drawable.fav_orange);
                    isFav = false;
                    isFavorite = "1";
                }else {
                    cb_favourite.setImageResource(R.drawable.fav);
                    isFav = true;
                    isFavorite = "0";
                }
            }
        });

        btnPrivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnPrivate.setBackgroundResource(R.drawable.button_bg_cyan);
                btnPublic.setBackgroundResource(R.drawable.button_bg);
                btnPrivate.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cyan_private, 0, 0, 0);
                btnPublic.setCompoundDrawablesWithIntrinsicBounds(R.drawable.public_share, 0, 0, 0);
                isPrivate = "1";
            }
        });

        btnPublic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnPublic.setBackgroundResource(R.drawable.button_bg_cyan);
                btnPrivate.setBackgroundResource(R.drawable.button_bg);
                btnPrivate.setCompoundDrawablesWithIntrinsicBounds(R.drawable.lock, 0, 0, 0);
                btnPublic.setCompoundDrawablesWithIntrinsicBounds(R.drawable.cyan_public, 0, 0, 0);
                isPrivate = "0";
            }
        });

        cb_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), Constant.snazlBitmap,"Snazy Image", null);
                Uri bmpUri = Uri.parse(pathofBmp);
                final Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                shareIntent.setType("image/*");
                startActivity(Intent.createChooser(shareIntent, "Share image using"));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_share_snazl, menu);
        MenuItem done = menu.findItem(R.id.action_done);
        done.setVisible(true);
        done.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_done) {

             String s=tv_links.getText().toString().trim();

             if (s.equals("No Url")){

                 stnrulr="";
             }else {

                 stnrulr=s;
             }

          //  Iterator myVeryOwnIterator = Constant.textsmap.keySet().iterator();


         //   new CreateSnazl().execute();

            new UplodSnazlImage().execute();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class UplodSnazlImage extends AsyncTask<String, Void, Void> {

        String status,message;


        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            if (nw.isConnectingToInternet()){
                try{

                    ServiceHandler sh  = new ServiceHandler();

                    JSONObject commentAdd = new JSONObject();

                    String newUrl = Constant.snazlUrl +"addfinalsnazl.php?&snazlid="+snazl_id+"&weblink="+stnrulr+"&isPrivate="+isPrivate+"&snazl_user="+user_id+"&isfav="+isFavorite;

                    System.out.println("upload url : "+newUrl);

                    response = sh.uploadImageFile(newUrl,String.valueOf(file_key),"image",commentAdd.toString(),file_key.getName());

                    JSONObject js = new JSONObject(response.body().string());
                    status = js.getString("status");
                    Log.e("status add image", status);

                    if (status.equals("Fail")){

                        message = js.getString("message");

                    }

                }catch (Exception ex){
                    ex.printStackTrace();
                    noData = true;
                }
                netConnection=true;
            }else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();

            if (!netConnection){
                Toast toast = Toast.makeText(getApplicationContext(), "Internet is not available. Connect to Internet.", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }else {
                if (noData){
                    Toast toast = Toast.makeText(getApplicationContext(), "Server Error!!!.", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }else {
                    if (status.equalsIgnoreCase("success")){

                        Intent intent=new Intent(ShareSnazlActivity.this,MainActivity.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        startActivity(intent);
                        finish();


                        Toast toast = Toast.makeText(getApplicationContext(), "Snazl created", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }else {

                        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }
                }
            }

            super.onPostExecute(aVoid);
        }
    }


}
