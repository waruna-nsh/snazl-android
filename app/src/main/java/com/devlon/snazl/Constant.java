package com.devlon.snazl;

import android.graphics.Bitmap;
import android.widget.ImageView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 4/9/2016.
 */
public class Constant {

    public static int iv_x;
    public static int iv_y;
    public static int iv_w;
    public static int iv_h;


    public static ImageView ivBackground;

    public static ArrayList<String> temp_interests = new ArrayList<>();

    public static String apiUrl = "http://snazl.devloninfotech.net/api/api.php";
    public static String snazlUrl = "http://snazl.devloninfotech.net/api/";

    public static final String FIREBASE_URL = "https://snazl-52930.firebaseio.com/";

    public static Bitmap snazlBitmap;

    //Create Snazl API

    public static String snazeltitle;
    public static String category_id;
    public static String user_id;
    public static String weblink;
    public static String bg_type;
    public static String bg_color;
    public static String bg_image;
    public static String bg_x;
    public static String bg_y;
    public static String pinch_ratio;
//    public static String isPrivate;

    //text jsonarray
    public static String text_title;
    public static String text_cordinate_x;
    public static String text_cordinate_y;
    public static String text_color;
    public static String text_font_style;
    public static String text_rotation;
    public static String text_height;
    public static String text_weight;


    public static String text_typeface_name;

    public  static HashMap<String,AutoResizeTextView> textsmap = new HashMap<String,AutoResizeTextView>();
    public  static HashMap<String,ImageView> imagemap = new HashMap<String,ImageView>();

    public static JSONArray textArray;
}
