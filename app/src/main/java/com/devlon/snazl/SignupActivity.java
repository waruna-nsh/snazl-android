package com.devlon.snazl;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.method.PasswordTransformationMethod;
import android.text.method.SingleLineTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class SignupActivity extends Activity {
    ImageView iv_cancel;
    EditText et_email, et_password,et_username;
    Button btnSignup;
    TextView tv1, tv2;
    Typeface custom_font, font_login;
    ProgressDialog prgDialog;
    NetworkConnection nw;
    Boolean netConnection = false;
    String userEmail, userPassword, userName;
    SessionManager sm;

    private FirebaseAuth firebaseAuth;


    String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


     String paswordpattern= "^(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        nw = new NetworkConnection(SignupActivity.this);
        sm = new SessionManager(SignupActivity.this);

        firebaseAuth = FirebaseAuth.getInstance();

        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Please Wait...");
        prgDialog.setCancelable(false);

        iv_cancel = (ImageView) findViewById(R.id.iv_cancel);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        et_username = (EditText) findViewById(R.id.et_username);
        btnSignup = (Button) findViewById(R.id.btnSignup);

        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);

        custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        font_login = Typeface.createFromAsset(getAssets(), "fonts/ufonts.com_schulbuchnord-fett.ttf");

        tv1.setTypeface(custom_font);
        tv2.setTypeface(custom_font);


        btnSignup.setTypeface(custom_font);
        et_email.setTypeface(custom_font);
        et_email.setTransformationMethod(new SingleLineTransformationMethod());
        et_password.setTypeface(custom_font);
        et_password.setTransformationMethod(new PasswordTransformationMethod());

        et_username.setTypeface(custom_font);
//        et_username.setTransformationMethod(new PasswordTransformationMethod());

        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                userName = et_username.getText().toString().trim();
                userEmail = et_email.getText().toString().trim();
                userPassword = et_password.getText().toString().trim();

                if (userName.isEmpty()){

                   Toast.makeText(getApplicationContext(), "Please enter username.", Toast.LENGTH_SHORT).show();

                }else if (userEmail.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Please enter email.", Toast.LENGTH_SHORT).show();

                }else if (!userEmail.matches(emailPattern)){

                    Toast.makeText(getApplicationContext(), "Please enter valid email.", Toast.LENGTH_SHORT).show();

                }else if (userPassword.isEmpty()){

                    Toast.makeText(getApplicationContext(), "Please enter password.", Toast.LENGTH_SHORT).show();

                }else if (userPassword.length()<8){

                    Toast.makeText(getApplicationContext(), "Your password must include at least 8 or more characters long", Toast.LENGTH_SHORT).show();

                }else if (!userPassword.matches(paswordpattern)){

                    Toast.makeText(getApplicationContext(), "Your password must include at least one symbol.", Toast.LENGTH_SHORT).show();

                }else {

                    //new SignUpOperation().execute();
                    firebaseAuth.createUserWithEmailAndPassword(userEmail, userPassword)
                            .addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (!task.isSuccessful()) {
                                        Log.d(TAG, "Authentication failed." + task.getException());

                                    } else {

                                        Toast toast = Toast.makeText(getApplicationContext(), "Thank you for signing up.", Toast.LENGTH_LONG);
                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                        toast.show();

                                        Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }

                                }
                            });
                }

               /* if (ValidateUser.isNotNull(userEmail) && ValidateUser.isNotNull(userPassword)&& ValidateUser.isNotNull(userName)) {
                    if (ValidateUser.validate(userEmail)) {

                        new SignUpOperation().execute();

                    } else {

                        Toast toast = Toast.makeText(getApplicationContext(), "Please enter valid email address.", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Please fill entire form to proceed.", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }*/
            }
        });

    }

    public class SignUpOperation extends AsyncTask<String, Void, Void> {

        String status, message;
        String uid,uname,uemail;

        @Override
        protected void onPreExecute() {
            prgDialog.show();
        }


        @Override
        protected Void doInBackground(String... strings) {

            if (nw.isConnectingToInternet() == true) {
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    nameValuePairs.add(new BasicNameValuePair("method", "register"));
                    nameValuePairs.add(new BasicNameValuePair("name", userName));
                    nameValuePairs.add(new BasicNameValuePair("email", userEmail));
                    nameValuePairs.add(new BasicNameValuePair("password", userPassword));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.apiUrl, ServiceHandler.GET,
                            nameValuePairs);

                    Log.e("response", response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");
                    if (status.contains("Fail")) {
                        message = js.getString("message");
                    }else {

                        uid = js.getString("user_id");
                        uname = js.getString("user_name");
                        uemail = js.getString("user_email_address");
                    }

                } catch (Exception ex) {

                }
                netConnection = true;
            } else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (netConnection == false) {
                Toast toast = Toast.makeText(getApplicationContext(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (status.contains("Success")) {

                    sm.UserSession(uid,uemail,uname);
                    sm.isLoggedIn();

                    Toast toast = Toast.makeText(getApplicationContext(), "Thank you for signing up.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
            super.onPostExecute(result);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();


    }
}
