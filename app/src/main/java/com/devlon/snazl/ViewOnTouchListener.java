package com.devlon.snazl;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

class ViewOnTouchListener implements View.OnTouchListener {
    Point pushPoint;
    int lastImgLeft;
    int lastImgTop;
    FrameLayout.LayoutParams viewLP;
    FrameLayout.LayoutParams pushBtnLP;
    int lastPushBtnLeft;
    int lastPushBtnTop;
    int lastPushBtnRight;
    int lastPushBtnTop_L;
    private View mPushView;
    private AutoResizeTextView editTextView;
    private ImageView iv_fadein;
    Context context;
    Rect rect;
    Rect textrect;
    View v;
    AnimationDrawable rocketAnimation;

    ViewOnTouchListener(Context context, View mPushView, AutoResizeTextView editTextView, ImageView iv_fadein) {
        this.mPushView = mPushView;
        this.context = context;
        this.editTextView = editTextView;
        this.iv_fadein = iv_fadein;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                v = Constant.ivBackground;
                rect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                if (null == viewLP) {
                    viewLP = (FrameLayout.LayoutParams) view.getLayoutParams();
                }
                if (null == pushBtnLP) {
                    pushBtnLP = (FrameLayout.LayoutParams) mPushView.getLayoutParams();
                }
                pushPoint = getRawPoint(event);
                lastImgLeft = viewLP.leftMargin;
                lastImgTop = viewLP.topMargin;
                lastPushBtnLeft = pushBtnLP.leftMargin;
                lastPushBtnTop = pushBtnLP.topMargin;

                break;

            case MotionEvent.ACTION_MOVE:
                Point newPoint = getRawPoint(event);
                float moveX = newPoint.x - pushPoint.x;
                float moveY = newPoint.y - pushPoint.y;




                viewLP.leftMargin = (int) (lastImgLeft + moveX);
                viewLP.topMargin = (int) (lastImgTop + moveY);
                view.setLayoutParams(viewLP);

                pushBtnLP.leftMargin = (int) (lastPushBtnLeft + moveX);
                pushBtnLP.topMargin = (int) (lastPushBtnTop + moveY);
                mPushView.setLayoutParams(pushBtnLP);

                System.out.println("Bounds Constant : " + Constant.iv_x + " , " + Constant.iv_y + " , " + Constant.iv_w + " , " + Constant.iv_h);
                System.out.println("Bounds Dragged : " + moveX + " , " + moveY );
                System.out.println("Bounds Points : " + viewLP.leftMargin + " , " + viewLP.topMargin );



               // Log.d("positionbound", "edittext cord:" + moveX + " rightlimit:" + right);
               // Log.d("positionbound", "editext paddin cord:" + view.getRight() + " rightlimit:" + (Constant.ivBackground.getWidth()+Constant.ivBackground.getRight())+" width :"+Constant.ivBackground.getWidth());


                Constant.ivBackground.setBackgroundColor(Color.RED);
                int[] l = new int[2];
                view.getLocationOnScreen(l);

                Log.d("cords", String.valueOf(view.getLeft()) + "-" + String.valueOf(l[0]) + " parant view:" + Constant.ivBackground.getLeft());


                AutoResizeTextView editext = (AutoResizeTextView)view;
                textrect = new Rect();
                Paint p = editext.getPaint();
                p.getTextBounds(editext.getText().toString(),0,editext.getText().length(),textrect);

                Log.d("stringpos", String.valueOf(rect.width()));
                Log.d("stringposview",String.valueOf(view.getWidth()));
                /*//if edittext text is out of right
                if(view.getLeft()>(Constant.ivBackground.getLeft()/2+Constant.ivBackground.getWidth())){
                    Log.d("positionbound1", "is out of right view");
                    mPushView.setVisibility(View.GONE);
                    editTextView.setVisibility(View.GONE);
                }
                else if(view.getLeft()<Constant.ivBackground.getLeft())
                {
                    Log.d("positionbound1","is out of left view");
                    mPushView.setVisibility(View.GONE);
                    editTextView.setVisibility(View.GONE);
                }*/



                break;

            case MotionEvent.ACTION_UP:
                //if edittext text is out of right



                //setMap
              Log.d("viewtag",view.getTag().toString());
                Constant.textsmap.put(view.getTag().toString(),((AutoResizeTextView)view));

                int viewcenterpos = view.getLeft()+(view.getWidth()/2);

                if((viewcenterpos)>(Constant.ivBackground.getLeft()+Constant.ivBackground.getWidth())){

                    Constant.textsmap.remove(view.getTag().toString());

                    Log.d("positionbound1", "is out of right view");
                    mPushView.setVisibility(View.GONE);
                    mPushView.setVisibility(View.GONE);
                    editTextView.setVisibility(View.GONE);
                    iv_fadein.setVisibility(View.VISIBLE);
                    iv_fadein.setImageResource(R.drawable.fading);
                    rocketAnimation = (AnimationDrawable) iv_fadein.getDrawable();
                    rocketAnimation.start();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv_fadein.setVisibility(View.GONE);
                        }
                    }, 2000);
                }
                else if(viewcenterpos<Constant.ivBackground.getLeft())
                {
                    Log.d("positionbound1", "is out of left view");
                    mPushView.setVisibility(View.GONE);
                    mPushView.setVisibility(View.GONE);
                    editTextView.setVisibility(View.GONE);
                    iv_fadein.setVisibility(View.VISIBLE);
                    iv_fadein.setImageResource(R.drawable.fading);
                    rocketAnimation = (AnimationDrawable) iv_fadein.getDrawable();
                    rocketAnimation.start();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            iv_fadein.setVisibility(View.GONE);
                        }
                    }, 2000);
                }
                break;

        }
        return false;
    }


    private Point getRawPoint(MotionEvent event) {
        return new Point((int) event.getRawX(), (int) event.getRawY());
    }
}
