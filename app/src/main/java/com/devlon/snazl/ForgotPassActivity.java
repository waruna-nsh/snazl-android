package com.devlon.snazl;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.SingleLineTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ForgotPassActivity extends Activity {

    ImageView iv_cancel;
    Button btnSubmit;
    EditText et_email;

    TextView tv1, tv2,tv_forgot_text;


    Typeface custom_font, font_login;

    ProgressDialog prgDialog;
    NetworkConnection nw;
    Boolean netConnection = false;
    String userEmail;
    SessionManager sm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);

        nw = new NetworkConnection(ForgotPassActivity.this);
        sm = new SessionManager(ForgotPassActivity.this);

        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Please Wait...");
        prgDialog.setCancelable(false);

        iv_cancel = (ImageView) findViewById(R.id.iv_cancel);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        et_email = (EditText) findViewById(R.id.et_email);

        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv_forgot_text = (TextView) findViewById(R.id.tv_forgot_text);

        custom_font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        font_login = Typeface.createFromAsset(getAssets(), "fonts/ufonts.com_schulbuchnord-fett.ttf");

        tv1.setTypeface(custom_font);
        tv2.setTypeface(custom_font);
        tv_forgot_text.setTypeface(custom_font);

        btnSubmit.setTypeface(custom_font);

        et_email.setTypeface(custom_font);
        et_email.setTransformationMethod(new SingleLineTransformationMethod());



        iv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userEmail = et_email.getText().toString().trim();
                if (ValidateUser.isNotNull(userEmail)) {
                    if (ValidateUser.validate(userEmail)) {
                        new ForgotPassword().execute();
                    } else {
                        Toast toast = Toast.makeText(getApplicationContext(), "Please enter valid email address.", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Please fill entire form to proceed.", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });

    }

    public class ForgotPassword extends AsyncTask<String, Void, Void> {

        String status, message;

        @Override
        protected void onPreExecute() {
            prgDialog.show();
        }


        @Override
        protected Void doInBackground(String... strings) {

            if (nw.isConnectingToInternet() == true) {
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    nameValuePairs.add(new BasicNameValuePair("method", "forgotpassword"));
                    nameValuePairs.add(new BasicNameValuePair("email", userEmail));

                    ServiceHandler sh = new ServiceHandler();
                    String response = sh.makeServiceCall(Constant.apiUrl, ServiceHandler.GET,
                            nameValuePairs);

                    Log.e("response", response);

                    JSONObject js = new JSONObject(response);
                    status = js.getString("status");
                    if (status.contains("Fail")) {
                        message = js.getString("message");
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                netConnection = true;
            } else {
                netConnection = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            prgDialog.dismiss();

            if (netConnection == false) {
                Toast toast = Toast.makeText(getApplicationContext(), "Internet is not available. Please turn on and try again.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            } else {
                if (status.contains("Success")) {

                    Toast toast = Toast.makeText(getApplicationContext(), "New password has been sent your email. Please check your inbox for password.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                    Intent intent = new Intent(ForgotPassActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
            super.onPostExecute(result);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(ForgotPassActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
