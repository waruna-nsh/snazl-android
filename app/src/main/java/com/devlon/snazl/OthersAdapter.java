package com.devlon.snazl;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

/**
 * Created by Devlon Android 2 on 05-Oct-16.
 */
public class OthersAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> textInterestLists;
    ArrayList<SnazlPojo> mySnazlLists;
    ImageLoader imageLoader;
    DisplayImageOptions options;

    public OthersAdapter(Context context, ArrayList<SnazlPojo> mySnazlLists){
        this.context = context;
        this.mySnazlLists = mySnazlLists;
    }

    @Override
    public int getCount() {
        return mySnazlLists.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder{
        ImageView iv_snazl_image;
        ImageView iv_snazl_invisible;
        TextView tv_snazl_text;
        ProgressBar download;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        final ViewHolder holder;
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.single_snazzy, null);
            holder = new ViewHolder();

            holder.tv_snazl_text = (TextView) convertView.findViewById(R.id.tv_snazl_text);
            holder.iv_snazl_image = (ImageView) convertView.findViewById(R.id.iv_snazl_image);
            holder.iv_snazl_invisible = (ImageView) convertView.findViewById(R.id.iv_snazl_invisible);
            holder.download = (ProgressBar) convertView.findViewById(R.id.download);

            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        SnazlPojo sp = mySnazlLists.get(position);

        holder.tv_snazl_text.setText(sp.getSnazl_title());

        String str_image=sp.getSnazl_url();


        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();
        ImageLoader.getInstance().init(config);

        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .resetViewBeforeLoading(true).build();

        imageLoader.getInstance().displayImage(str_image, holder.iv_snazl_invisible, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.download.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                String message = null;
                switch (failReason.getType()) {
                    case IO_ERROR:
                        //message = "Input/Output error";
                        System.out.println("IO_ERROR");
                        //  holder.img_Product.setImageResource(R.drawable.no_image);
                        break;
                    case DECODING_ERROR:
                        //message = "Image cannot be decoded...";
                        System.out.println("DECODING_ERROR");
                        //  holder.img_Product.setImageResource(R.drawable.no_image);
                        break;
                    case NETWORK_DENIED:
                        //message = "Downloads are denied";
                        System.out.println("NETWORK_DENIED");
                        // holder.img_Product.setImageResource(R.drawable.no_image);
                        break;
                    case OUT_OF_MEMORY:
                        //message = "Out Of Memory error";
                        System.out.println("OUT_OF_MEMORY");
                        //  holder.img_Product.setImageResource(R.drawable.no_image);
                        break;
                    case UNKNOWN:
                        //message = "Unknown error";
                        System.out.println("UNKNOWN");
                        // holder.img_Product.setImageResource(R.drawable.no_image);
                        break;
                }
                holder.download.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                //super.onLoadingComplete(imageUri, view, loadedImage);
                holder.download.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                super.onLoadingCancelled(imageUri, view);
            }
        });


        return convertView;
    }
}
